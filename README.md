# Codebase release 1.0 for BlockGen

by Enrico Bothmann, Walter Giele, Stefan Hoeche, Joshua Isaacson, Max Knobbe

SciPost Phys. Codebases 3-r1.0 (2022) - published 2022-08-23

[DOI:10.21468/SciPostPhysCodeb.3-r1.0](https://doi.org/10.21468/SciPostPhysCodeb.3-r1.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.3-r1.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Enrico Bothmann, Walter Giele, Stefan Hoeche, Joshua Isaacson, Max Knobbe

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.3-r1.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.3-r1.0)
* Live (external) repository at [https://gitlab.com/ebothmann/blockgen-archive/-/tree/v1.0.1](https://gitlab.com/ebothmann/blockgen-archive/-/tree/v1.0.1)

# BlockGen

Code based on [Many-gluon tree amplitudes on modern GPUs: A case study for novel event generators](https://arxiv.org/abs/2106.06507)

The code contains the calculations for:
1. Leading Color
2. [TESS](https://doi:10.1140/epjc/s10052-011-1703-5)
3. Color Ordered and sampled
4. Color Ordered and summed
5. Color Dressed and sampled

Please note, that we only provide the color-matrices for 2,3,4 outgoing Gluons. For higher multiplicities, please contact the authors. 

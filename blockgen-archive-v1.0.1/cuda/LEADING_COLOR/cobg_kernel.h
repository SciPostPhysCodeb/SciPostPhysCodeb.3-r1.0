//
// COBG kernel
//

#include "config.h"
#include "../../tools/vec4.h"

__device__ void e_real(vec4 *J, const vec4 &p, const real ran, const int Jind){
  // Constructs random real polarisation vector
  // the random numbers are starting with rans[rseed] and incremented by rinc
  //
  // needs 3 random number in [0,1] for each polarisation vector
  //
  
  real k[4];
  k[0] = 0;
  k[1] = p[1];
  k[2] = p[2];
  k[3] = p[3];
  real norm = 1.*sqrt(pow(k[1],2)+pow(k[2],2)+pow(k[3],2));
  for(int i = 1; i<4; i++){
    k[i] /= norm;
  }
  // order momenta
  int xi[4] = {0,1,2,3};
  int maxind = 1;
  for(int i = 2; i<4; i++){
    maxind = (std::abs(p[i]) > std::abs(p[maxind])) ? i : maxind;
  }
  xi[maxind] = 1;
  xi[1] = maxind;
  norm = sqrt(k[xi[1]]*k[xi[1]] + k[xi[2]]*k[xi[2]]);

  // construct normalised vector orthogonal to k
  real r[4];
  r[0] = 0;
  r[xi[1]] =  k[xi[2]];
  r[xi[2]] = -k[xi[1]];
  r[xi[3]] = 0;
  for(int i = 1; i<4; i++){
    r[i] /= norm;
  }

  // Construct vector orthogonal to k and r
  real s[4];
  s[0] = 0;
  s[xi[1]] = k[xi[1]] * k[xi[3]] / norm;
  s[xi[2]] = k[xi[2]] * k[xi[3]] / norm;
  s[xi[3]] = -norm;

  // compute cos theta and sin theta at the same time  
  real costheta;
  real sintheta;
  real theta = 2.*ran* M_PI;
  //theta = 1.0;
  sincos(theta, &sintheta, &costheta);
  int tid = (threadIdx.x + blockDim.x*blockIdx.x);
    
  if(tid == 0){
    //printf("s alt= %f %f %f %f\n", s[0],s[1],s[2],s[3]);
  }
  
  J[Jind] = {0,
	     costheta*r[1] + sintheta * s[1],
	     costheta*r[2] + sintheta * s[2],
	     costheta*r[3] + sintheta * s[3]};
}

__device__ void printVec4(const vec4 v){
  printf("(%f, %f, %f, %f)", v[0],v[1],v[2],v[3]);
}

__device__ inline int index(const int k, const int l){
  int m = l-k+1;
  return ((m-1)*(n-m/2.) + k);
}

__device__ vec4 V3(const vec4 Ja, const vec4 Jb, const vec4 Pa, const vec4 Pb){
  return  2*(Ja*Pb)*Jb - 2*(Pa*Jb)*Ja + (Ja*Jb)*(Pa-Pb);
}

__device__ vec4 V4(const vec4 Ja, const vec4 Jb, const vec4 Jc){
  return 2*(Ja*Jc)*Jb - (Ja*Jb)*Jc - (Jc*Jb)*Ja;
}

__global__ void init_P(vec4 *P, const real *p, const int *shuffle){
  int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  int indexx;

  // Fill momenta
  for(int i=0; i<n-1; i++){
    indexx = shuffle[tid + i*d_nev];
    for(int j=0; j<4; j++){
      P[tid + i*d_nev].data[j] = p[tid + (indexx*4+j)*d_nev];
    }
  }
  indexx = shuffle[tid + (n-1)*d_nev];
  for(int j=0; j<4; j++){
    P[tid + NN*d_nev].data[j] = p[tid + (indexx*4+j)*d_nev];
  }
  for(int m=1; m<n; m++){
    for(int k=0; k<n-m-1; k++){
      P[tid + index(k,k+m)*d_nev] = P[tid+index(k,k)*d_nev] + P[tid+index(k+1,k+m)*d_nev];
    }
  }
}

__global__ void cobg(vec4 *J, vec4 *PP, const real *p, const real *pol_vectors, const int *pols, int *shuffle){
  // unique id of the thread, for d_nev events, tid ranges from [0,d_nev)
  // Thus we set up one thread for each event
  int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  
  // fill currents
  int pol_index;
  int perm;
  int indexx;

  extern __shared__ vec4 PPP[];
  vec4 *P = &PPP[threadIdx.x * (n+1)];
  
  // Fill momenta
  indexx = shuffle[tid + 0*d_nev];
  for(int j=0; j<4; j++){
    P[0].data[j] = 0;
    P[1].data[j] = p[tid + (indexx*4+j)*d_nev];
  }

  for(int i=1; i<n; i++){
    indexx = shuffle[tid + i*d_nev];
    for(int j=0; j<4; j++){
      P[i+1].data[j] = P[i][j] + p[tid + (indexx*4+j)*d_nev];
    }
  }

  for(int i=0; i<n-1; i++){
    perm = shuffle[tid + i*d_nev];
    pol_index  = tid;          // move to right column
    pol_index += perm*8*d_nev; // move to right start of pol vectors
    pol_index += pols[tid + perm*d_nev]*d_nev; // choose right polarisation
    
    for( int j = 0; j<4; j++){
      J[tid + (i*d_nev)][j] = pol_vectors[pol_index + 2*j*d_nev];
    }
  }
  
  for( int j = 0; j<4; j++){
    J[tid + ((NN + 0)*d_nev)][j] = pol_vectors[tid + ((n-1)*8 + 2*j+0)*d_nev];
    J[tid + ((NN + 1)*d_nev)][j] = pol_vectors[tid + ((n-1)*8 + 2*j+1)*d_nev];
  }
  
  vec4 current;
  for(int m=1; m<n-2; m++){
    for(int k=0; k<n-m-1; k++){
      current = V3(J[tid+index(k,k)*d_nev],
		   J[tid+index(k+1,k+m)*d_nev],
		   P[k+1] - P[k],
		   P[k+m+1] - P[k+1]);
      
      for(int i=k+1; i<k+m; i++){
	current += V3(J[tid+index(k,i)*d_nev],
		      J[tid+index(i+1,k+m)*d_nev],
		      P[i+1] - P[k],
		      P[k+m+1] - P[i+1]);
      }
      for(int i=k; i<k+m; i++){
	for(int j = i+1; j<k+m;j++){
	  current += V4(J[tid + index(k,i)*d_nev],
			J[tid + index(i+1,j)*d_nev],
			J[tid + index(j+1,k+m)*d_nev]);
	}	
      }
      J[tid + index(k,k+m)*d_nev] = current/((P[k+m+1] - P[k])*(P[k+m+1]-P[k]));
    }
  }
  
  for(int m=n-2; m<n; m++){
    for(int k=0; k<n-m-1; k++){
      current = V3(J[tid+index(k,k)*d_nev],
		   J[tid+index(k+1,k+m)*d_nev],
		   P[k+1] - P[k],
		   P[k+m+1] - P[k+1]);
      for(int i=k+1; i<k+m; i++){
	current += V3(J[tid+index(k,i)*d_nev],
		      J[tid+index(i+1,k+m)*d_nev],
		      P[i+1]-P[k],
		      P[k+m+1]-P[i+1]);
      }
      for(int i=k; i<k+m; i++){
	for(int j = i+1; j<k+m;j++){
	  current += V4(J[tid + index(k,i)*d_nev],
			J[tid + index(i+1,j)*d_nev],
			J[tid + index(j+1,k+m)*d_nev]);
	}	
      }
      J[tid + index(k,k+m)*d_nev] = current;
    }
  }
  //A[tid] = J[tid + NN*d_nev]*J[tid + (NN-1)*d_nev];
}

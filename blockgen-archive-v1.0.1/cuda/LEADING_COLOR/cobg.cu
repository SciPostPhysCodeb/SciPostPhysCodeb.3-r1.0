#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include <curand.h>
#include <string>
#include <fstream>
#include <chrono>
#include "../tools/helper_cuda.h"
#include "cuda_runtime.h"
#include <getopt.h>

// Global constants avaiable on the device
__constant__ int nin;
__constant__ int nout;
__constant__ real ecm;
__constant__ int d_nev;
__constant__ int NN;
__constant__ int n;

// Include kernel functions
#include "../tools/vec4.h"
#include "../kernel/rambo_kernel.h"
#include "../kernel/cut_kernel.h"
#include "../kernel/shuffle_kernel.h"
#include "../kernel/monte_carlo_kernel.h"

// CPU functionality
#include "../tools/tools.h"
#include "../tools/system.h"

// Local kernel
#include "cobg_kernel.h"

__global__ void fillPolarisations(int *pols, const unsigned int npol){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  for(unsigned int i = 0; i<n; i++){
    pols[tid + i*d_nev] = (npol & (1 << i)) >> i;
  }
}
__global__ void fillPolarisationsRandom(int *pols, const real *rans){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  for(unsigned int i = 0; i<n; i++){
    pols[tid + i*d_nev] = static_cast<int>(2*rans[tid+i*d_nev]);
  }
}

__global__ void add_CaseHelicitySumming(real *A, const vec4 *J, const real quot){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  A[tid] += pow(J[tid + (NN + 0)*d_nev]*J[tid + (NN-1)*d_nev],2) / quot;
  A[tid] += pow(J[tid + (NN + 1)*d_nev]*J[tid + (NN-1)*d_nev],2) / quot;
}

__global__ void add_CaseHelicitySampling(real *A, const vec4 *J, const int *pols){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  A[tid] += pow(J[tid + (NN + pols[tid + (n-1)*d_nev])*d_nev]*J[tid + (NN-1)*d_nev],2);
}

int print_short_help_and_exit(int argc, char **argv){
  std::cout << "Usage: " << argv[0]
            << " -n <nout> -e <nevt_power>"
	    << " [-b <nblocks> -s <blocksize> -d <gpu nr>"
	    << " --sum-hels --me2-only --profiling]\n";
  exit(1);
}

// Main Programm
//
int main(int argc, char **argv){
  if (argc < 5)
    print_short_help_and_exit(argc, argv);

  // user options
  int h_nout = 2;
  int sum_helicities = 0;
  int me2_only = 0;
  int sweeps = 0;
  int verbose = 0;
  int nblocks   = 640;
  int blocksize = 512;
  int device = 0;
  int profiling = 0;
  
  // parse environment variables
  std::string data_path = get_environment_variable("COBG_DATA_PATH", "../../dat");

  // parse cmdline options
  // adapted from https://www.informit.com/articles/article.aspx?p=175771&seqNum=3
  struct option longopts[] = {
    { "nout",     required_argument, NULL,            'n' },
    { "nev",      required_argument, NULL,            'e' },
    { "nblocks",  optional_argument, &nblocks,        'b' },
    { "blocksize",optional_argument, &blocksize,      's' },
    { "device",   optional_argument, &device,         'd' },
    { "sum-hels", no_argument,       &sum_helicities, 1   },
    { "me2-only", no_argument,       &me2_only,       1   },
    { "verbose",  no_argument,       &verbose,        1   },
    { "profiling",no_argument,       &profiling,      1   },
    { 0, 0, 0, 0 }
  };
  int getopt_err = 0;
  int c;

  while ((c = getopt_long(argc, argv, ":n:e:b:s:d:pmvg", longopts, NULL)) != -1) {
    switch (c) {
    case 'n':
      h_nout = atoi(optarg);
      break;
    case 'e':
      sweeps = atoi(optarg);
      break;
    case 'b':
      nblocks = atoi(optarg);
      break;
    case 's':
      blocksize = atoi(optarg);
      break;
    case 'd':
      device = atoi(optarg);
      break;
    case 'p':
      sum_helicities = 1;
      break;
    case 'm':
      me2_only = 1;
      break;
    case 'v':
      verbose = 1;
      break;
    case 'g':
      profiling = 1;
      break;
    case 0:     /* getopt_long() set a variable, just keep going */
      break;
#if 0
    case 1:
      /*
       * Use this case if getopt_long() should go through all
       * arguments. If so, add a leading '-' character to optstring.
       * Actual code, if any, goes here.
       */
      break;
#endif
    case ':':   /* missing option argument */
      fprintf(stderr, "%s: option `-%c' requires an argument\n",
	      argv[0], optopt);
      getopt_err = 1;
      break;
    case '?':
    default:    /* invalid option */
      fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
	      argv[0], optopt);
      getopt_err = 1;
      break;
    }
  }
  if (getopt_err)
    print_short_help_and_exit(argc, argv);
  
  real *h_moms, *d_moms, *d_wgts, *h_AA, *d_AA, *d_AA_tmp, *d_y, *d_phi;
  real *d_rans_x1, *d_rans_x2, *d_rans_rambo, *d_rans_cobg, *d_rans_shuf;
  bool *h_cuts, *d_cuts;
  real *d_r_scale, *d_f_scale;
  real *d_mc_results;
  vec4 *d_Jv, *d_Pv;
  
  int *d_shuffle;
  real sum = 0;
  real sum2 = 0;
  real max = 0;
  real accepted = 0;
  real dcs;

  const int h_nin = 2;
  const real h_ecm = 14000;
  const int nev = blocksize*nblocks;
  const int h_nn = (h_nout+h_nin-1)*(h_nout+h_nin)/2.;
  const int h_n = h_nin + h_nout;
  const int gammaprod = fact(h_nout-1) * fact(h_nout-2);

  // choose GPU
  cudaSetDevice(device) ;
  
  // Transfer variables to device
  checkCudaErrors( cudaMemcpyToSymbol(nin,   &h_nin,   sizeof(int)));
  checkCudaErrors( cudaMemcpyToSymbol(nout,  &h_nout,  sizeof(int)));
  checkCudaErrors( cudaMemcpyToSymbol(d_nev, &nev,     sizeof(int)));
  checkCudaErrors( cudaMemcpyToSymbol(NN,    &h_nn,    sizeof(int)));
  checkCudaErrors( cudaMemcpyToSymbol(n,     &h_n,     sizeof(int)));
  checkCudaErrors( cudaMemcpyToSymbol(ecm,   &h_ecm,   sizeof(real)));

  // allocate memory for the monte carlo results
  checkCudaErrors(cudaMalloc((void **)&d_mc_results, sizeof(real)*nev));
  
  // initialise CUDA timing
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  // allocate memory on host and device for the momenta
  h_moms = (real *)malloc(sizeof(real)*nev*4*(h_nin + h_nout));
  checkCudaErrors( cudaMalloc((void **)&d_moms, sizeof(real)*nev*4*(h_nin + h_nout)) );
  checkCudaErrors( cudaMalloc((void **)&d_wgts, sizeof(real)*nev) );
  
  // malloc space for the random numbers
  //
  // probably to combine all these into one array of combined lenght
  //    if random nuber generation ever becomes significant this should
  //    be the starting point
  //
  const int n_rans_xx    = nev;
  const int n_rans_rambo = 4*h_nout*nev;
  const int n_rans_cobg  = h_n*nev;
  const int n_rans_shuf  = (h_n-1)*nev;
  checkCudaErrors( cudaMalloc((void **)&d_rans_x1,    sizeof(real)*n_rans_xx) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_x2,    sizeof(real)*n_rans_xx) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_rambo, sizeof(real)*n_rans_rambo) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_cobg,  sizeof(real)*n_rans_cobg) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_shuf,  sizeof(real)*n_rans_shuf) );

  // alocate the squared amplitudes in the host and device
  h_AA = (real *)malloc(sizeof(real)*nev);
  checkCudaErrors( cudaMalloc((void **)&d_AA,     sizeof(real)*nev) );
  checkCudaErrors( cudaMalloc((void **)&d_AA_tmp, sizeof(real)*nev) );

  // Allocate memory on device for the Jcurrents and corresponding momenta  
  checkCudaErrors( cudaMalloc((void **)&d_Jv,       sizeof(vec4)*nev*(h_nn+2) ));
  checkCudaErrors( cudaMalloc((void **)&d_Pv,       sizeof(vec4)*nev*(h_nn+1) ));
  checkCudaErrors( cudaMalloc((void **)&d_shuffle,  sizeof(int)*nev*h_n ));
  checkCudaErrors( cudaMalloc((void **)&d_r_scale,  sizeof(real)*nev ));
  checkCudaErrors( cudaMalloc((void **)&d_f_scale,  sizeof(real)*nev ));
  
  // Storage for the precomputed polarisation vectors
  int  *d_pols;
  real *d_polarisation_vectors;
  checkCudaErrors( cudaMalloc((void **)&d_pols,                 sizeof(int) *nev*h_n));
  checkCudaErrors( cudaMalloc((void **)&d_polarisation_vectors, sizeof(real)*nev*h_n*2*4));
  
  // Allocate memory on host and device for the cuts
  h_cuts = (bool *)malloc(sizeof(bool)*nev);
  checkCudaErrors( cudaMalloc((void **)&d_cuts, sizeof(bool)*nev) );
  checkCudaErrors( cudaMalloc((void **)&d_y,    sizeof(real)*nev*h_n) );
  checkCudaErrors( cudaMalloc((void **)&d_phi,  sizeof(real)*nev*h_n) );
  
  // initialise random number generator
  curandGenerator_t gen;
  checkCudaErrors( curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT) );
  checkCudaErrors( curandSetPseudoRandomGeneratorSeed(gen, 0) );

  // cuda recommendations for blocksize and nblocks
  int minGridSize;
  int blockSize;
  const int sharedMemory = (h_n+1)*blocksize;

  cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, cobg, 0, 0);
  printf("\nCOMPUTATIONAL DETAILS:\n");
  printf("   Number of blocks    : %d\n", nblocks);
  printf("   Recommended         : %d\n", minGridSize);
  printf("   Threads / block     : %d\n", blocksize);
  printf("   Recommended         : %d\n", blockSize);
  printf("   Total No. threds    : %d\n", nblocks * blocksize);
  printf("   Total No. sweeps    : %d\n", sweeps);
  printf("   Number of events    : %.2g\n",1.*nev*sweeps);
  printf("   Shared mem requested: %d\n", sharedMemory*sizeof(vec4));

  
  float t_rans, t_cuts, t_rambo, t_cobg, t_mc, t_pdf, t_als, t_shuffle;
  bool print_progress = sweeps >= 10;
  print_progress = true;
  auto t0 = std::chrono::high_resolution_clock::now();
  if(print_progress){
    std::cout << "\nMONTE-CARLO PROGRESS:" << std::endl;
  }
    
  //
  // MONTE CARLO STARTS HERE
  //
  for(int j = 1; j<=sweeps; j++){    
    // generate all random numbers
    cudaEventRecord(start);
#ifdef USE_DOUBLE_PRECISION
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_x1,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_x2,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_rambo, n_rans_rambo) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_cobg,  n_rans_cobg) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_shuf,  n_rans_shuf) );
#else
    checkCudaErrors( curandGenerateUniform(gen, d_rans_x1,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_x2,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_rambo, n_rans_rambo) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_cobg,  n_rans_cobg) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_shuf,  n_rans_shuf) );
    set_min_max<<<nblocks, blocksize>>>(d_rans_x1,    n_rans_xx);
    set_min_max<<<nblocks, blocksize>>>(d_rans_x2,    n_rans_xx);
    set_min_max<<<nblocks, blocksize>>>(d_rans_rambo, n_rans_rambo);
    set_min_max<<<nblocks, blocksize>>>(d_rans_cobg,  n_rans_cobg);
    set_min_max<<<nblocks, blocksize>>>(d_rans_shuf,  n_rans_shuf);
#endif
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_rans, start, stop);
    
    // generate nev events using the rambo kernel
    cudaEventRecord(start);
    generatePoint<<<nblocks, blocksize>>>(d_moms, d_wgts, d_rans_rambo,
					  d_rans_x1, d_rans_x2, gammaprod);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    checkCudaErrors(cudaEventElapsedTime(&t_rambo, start, stop));

    // generate nev events using the rambo kernel
    cudaEventRecord(start);
    shuffle<<<nblocks, blocksize>>>(d_rans_shuf, d_shuffle);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_shuffle, start, stop);
    
    // check cuts and flag if outside cuts
    cudaEventRecord(start);
    cuts<<<nblocks, blocksize>>>(d_cuts, d_moms, d_y, d_phi, 20., 2.5, 0.4);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    checkCudaErrors(cudaEventElapsedTime(&t_cuts, start, stop));

    // Computing the squared amplitudes using the BG recursion relations
    cudaEventRecord(start);
    
    // MATRIX ELEMENT
    cudaMemset(d_AA    , 0, nev*sizeof(real));
    cudaMemset(d_AA_tmp, 0, nev*sizeof(real));
    fill_polarisation_vectors<<<nblocks, blocksize>>>(d_polarisation_vectors, d_moms);
    
    if(sum_helicities){
      // Polarisation sum
      for(unsigned int ii = 0; ii<pow(2,h_n-1); ii++){
	fillPolarisations<<<nblocks, blocksize>>>(d_pols, ii);
	cobg<<<nblocks, blocksize, sharedMemory*sizeof(vec4)>>>(d_Jv, d_Pv, d_moms,d_polarisation_vectors, d_pols, d_shuffle);
	add_CaseHelicitySumming<<<nblocks, blocksize>>>(d_AA, d_Jv, pow(2,h_n));
      }
    }
    else{
      // Polarisation sampling
      fillPolarisationsRandom<<<nblocks, blocksize>>>(d_pols, d_rans_cobg);
      cobg<<<nblocks, blocksize, sharedMemory*sizeof(vec4)>>>(d_Jv, d_Pv, d_moms, d_polarisation_vectors, d_pols, d_shuffle);
      add_CaseHelicitySampling<<<nblocks, blocksize>>>(d_AA, d_Jv, d_pols);
    }
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_cobg, start, stop);
    
    // combine all of the above
    cudaEventRecord(start);
    MonteCarloLeadingColor<<<nblocks, blocksize>>>(d_mc_results, d_rans_x1, d_rans_x2, d_AA, d_wgts, d_cuts);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_mc, start, stop);
    
    // copy back results to the host
    checkCudaErrors(cudaMemcpy(h_AA, d_mc_results, sizeof(real)*nev, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(h_cuts, d_cuts, sizeof(bool)*nev, cudaMemcpyDeviceToHost));
    real tmp  = 0;
    real tmp2 = 0;
    for(int i = 0; i<nev; i++){
      if (h_cuts[i]){
	if (h_AA[i] > max){
	  max = h_AA[i];
	}
	dcs = h_AA[i];
	tmp += dcs;
	tmp2 += dcs*dcs;
	accepted += h_cuts[i];
      }
    }
    sum += tmp / (1.*nev);
    sum2 += tmp2 / (1.*nev);
    if (print_progress && j % 10 == 0){
      auto t_tmp = std::chrono::high_resolution_clock::now();
      auto t_diff = t_tmp - t0;
      auto t_millis = std::chrono::duration_cast<std::chrono::milliseconds>(t_diff).count();
      std::cout << "\r   " << std::ceil(j/float(sweeps)*100) << " %" 
		<< ", passed: " << int(t_millis/1000) << " s, remaining: "
		<< int(t_millis/1000. / j * (sweeps - j )) << " s              " 
		<< std::flush;
    }
  } // end of sweep for loop
  
  // Summary + Final computations
  printf("\n\nPROCESS:\n");
  printf("   Number of particles : %d\n", h_nin+h_nout);

  printf("\nPERFORMANCE SUMMARY:\n");
  
  float t_Tot = t_rans + t_cuts + t_rambo + t_cobg + t_mc;
  printf("   Process \t Total Time [ms] \t Time/event [s] \t Frac of Total Time [%]\n");
  printf("   ----------------------------------------------------------------------------------- \n");
  printsummary("Rans  ", t_rans,    t_Tot, nev);
  printsummary("Rambo ", t_rambo,   t_Tot, nev);
  printsummary("Shuf. ", t_shuffle, t_Tot, nev);
  printsummary("Cuts  ", t_cuts,    t_Tot, nev);
  printsummary("Cobg  ", t_cobg,    t_Tot, nev);
  printsummary("MC    ", t_mc,      t_Tot, nev);
  printsummary("Total:", t_Tot,     t_Tot, nev);

  real avg = sum / sweeps;
  real stddev = sqrt(sum2 / sweeps - pow(sum / sweeps, 2)) / sqrt(1.*nev * sweeps);  
  printf("\nRESULTS:\n");  
  printf("   |A|^2 = %.4e +/- (%.2e = %.3g %) \n", avg, stddev, stddev/avg*100);
  printf("   Max weight = %.4e\n", max);
  printf("   Sampling efficiency = %.3g %\n", 100 * accepted * 1. / (1. * sweeps * nev));
  
  // CUDA exit 
  // also free memory
  cudaFree(d_moms);
  cudaFree(d_wgts);
  cudaFree(d_rans_x1);
  cudaFree(d_rans_x2);
  cudaFree(d_rans_rambo);
  cudaFree(d_rans_cobg);
  cudaFree(d_AA);
  cudaFree(d_cuts);
  cudaFree(d_y);
  cudaFree(d_phi);
  free(h_moms);
  free(h_AA);
  free(h_cuts);
  cudaDeviceReset();
}

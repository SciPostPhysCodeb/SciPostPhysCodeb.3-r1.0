// These 3 GPU properties should be read-out from device.
//
/*
#define NUMBER_MP 1*30
#define SHARED_MEMORY 16384 // =2^14
#define REGISTER_MEMORY 16384 //=2^14
//
//
// Set the maximum number of registers ever used by program
//
*/
#define NUMBER_MP 56
#define SHARED_MEMORY 16384*3
#define REGISTER_MEMORY 16384
#define MAX_REGISTER 35




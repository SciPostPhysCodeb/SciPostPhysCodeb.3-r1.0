#ifndef VEC4H
#define VEC4H

#include <math.h>
#include <stdlib.h>
#include <iostream>
#include "config.h"

class vec4  {
 public:
  real data[4];
  __host__ __device__ vec4(){}
  __host__ __device__ vec4(real E, real px, real py, real pz){
    data[0] = E;
    data[1] = px;
    data[2] = py;
    data[3] = pz;
  }
  __host__ __device__ inline real E() { return data[0]; }
  __host__ __device__ inline real px(){ return data[1]; }
  __host__ __device__ inline real py(){ return data[2]; }
  __host__ __device__ inline real pz(){ return data[3]; }

  __host__ __device__ inline const vec4& operator+() const { return *this; }
  __host__ __device__ inline vec4 operator-() const {
    return vec4(-data[0], -data[1], -data[2], -data[3]); }
  __host__ __device__ inline real operator[](int i) const { return data[i]; }
  __host__ __device__ inline real& operator[](int i) { return data[i]; };

  // Four vector operations
  __host__ __device__ inline vec4& operator+=(const vec4 &v2);
  __host__ __device__ inline vec4& operator-=(const vec4 &v2);

  // Scalar operations
  __host__ __device__ inline vec4& operator*=(const real x);
  __host__ __device__ inline vec4& operator/=(const real x);

};

__host__ __device__ inline vec4& vec4::operator+=(const vec4 &v2){
  data[0] += v2.data[0];
  data[1] += v2.data[1];
  data[2] += v2.data[2];
  data[3] += v2.data[3];
  return *this;
}

__host__ __device__ inline vec4& vec4::operator-=(const vec4 &v2){
  data[0] -= v2.data[0];
  data[1] -= v2.data[1];
  data[2] -= v2.data[2];
  data[3] -= v2.data[3];
  return *this;
}

__host__ __device__ inline vec4& vec4::operator*=(const real x){
  data[0] *= x;
  data[1] *= x;
  data[2] *= x;
  data[3] *= x;
  return *this;
}
__host__ __device__ inline vec4& vec4::operator/=(const real x){
  data[0] /= x;
  data[1] /= x;
  data[2] /= x;
  data[3] /= x;
  return *this;
}

__host__ __device__ inline vec4 operator+(const vec4 &v1, const vec4 &v2){
  return vec4(v1.data[0] + v2.data[0],
	      v1.data[1] + v2.data[1],
	      v1.data[2] + v2.data[2],
	      v1.data[3] + v2.data[3]); 
}

__host__ __device__ inline vec4 operator-(const vec4 &v1, const vec4 &v2){
  return vec4(v1.data[0] - v2.data[0],
	      v1.data[1] - v2.data[1],
	      v1.data[2] - v2.data[2],
	      v1.data[3] - v2.data[3]); 
}

__host__ __device__ inline real operator*(const vec4 &v1, const vec4 &v2){
  return v1.data[0]*v2.data[0]
    - v1.data[1]*v2.data[1]
    - v1.data[2]*v2.data[2]
    - v1.data[3]*v2.data[3];
}

__host__ __device__ inline vec4 operator*(const vec4 &v1, const real x){
  return vec4(v1.data[0] * x,v1.data[1] * x,v1.data[2] * x,v1.data[3] * x); 
}

__host__ __device__ inline vec4 operator*(const real x,const vec4 &v1){
  return vec4(v1.data[0] * x,v1.data[1] * x,v1.data[2] * x,v1.data[3] * x); 
}

__host__ __device__ inline vec4 operator/(const vec4 &v1, const real x){
  return vec4(v1.data[0] / x,v1.data[1] / x,v1.data[2] / x,v1.data[3] / x); 
}

__host__ __device__ inline vec4 operator/(const real x, const vec4 &v1){
  return vec4(v1.data[0] / x,v1.data[1] / x,v1.data[2] / x,v1.data[3] / x); 
}

__global__ void fill_polarisation_vectors(real *Pols, const real *p){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);

  //
  // take care of the mallocs, in principle these could be smaller
  // 4-->3 but we need the larger arrays in case we ever include fermions
  // 
  real k[4], r[4], s[4], norm;
  int xi[4], maxind;
  
  for(int ii = 0; ii<n; ii++){
    k[0] = 0;
    k[1] = p[tid + (1+ii*4)*d_nev];
    k[2] = p[tid + (2+ii*4)*d_nev];
    k[3] = p[tid + (3+ii*4)*d_nev];
    norm = 1.*sqrt(pow(k[1],2)+pow(k[2],2)+pow(k[3],2));
    for(int i = 1; i<4; i++){
      k[i] /= norm;
      xi[i] = i;
    }
    
    // order momenta
    maxind = 1;
    for(int i = 2; i<4; i++){
      maxind = (abs(p[tid + (i+ii*4)*d_nev]) > abs(p[tid + (maxind+ii*4)*d_nev])) ? i : maxind;
    }
    xi[maxind] = 1;
    xi[1] = maxind;
    norm = sqrt(k[xi[1]]*k[xi[1]] + k[xi[2]]*k[xi[2]]);

    // construct normalised vector orthogonal to k
    r[0] = 0;
    r[xi[1]] =  k[xi[2]];
    r[xi[2]] = -k[xi[1]];
    r[xi[3]] = 0;
    for(int i = 1; i<4; i++){
      r[i] /= norm;
    }
    
    // Construct vector orthogonal to k and r
    s[0] = 0;
    s[xi[1]] = k[xi[1]] * k[xi[3]] / norm;
    s[xi[2]] = k[xi[2]] * k[xi[3]] / norm;
    s[xi[3]] = -norm;

    // Copy vectors
    Pols[tid + (ii*2*4 + 0)*d_nev ] = s[0];
    Pols[tid + (ii*2*4 + 1)*d_nev ] = r[0];
    Pols[tid + (ii*2*4 + 2)*d_nev ] = s[1];
    Pols[tid + (ii*2*4 + 3)*d_nev ] = r[1];
    Pols[tid + (ii*2*4 + 4)*d_nev ] = s[2];
    Pols[tid + (ii*2*4 + 5)*d_nev ] = r[2];
    Pols[tid + (ii*2*4 + 6)*d_nev ] = s[3];
    Pols[tid + (ii*2*4 + 7)*d_nev ] = r[3];
  }
}


#endif

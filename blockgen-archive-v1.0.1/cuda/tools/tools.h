#ifndef TOOLSH
#define TOOLSH

#include "config.h"
#include <iostream>
#include <math.h>
#include <cstdlib>

// creates random integers
// uniformly distributed in the range [i,j).
inline int getRand(const int i, const int j){
  return i + std::rand()*1./RAND_MAX *(j-i);
}

int fact(const int n){
  return (n == 1 || n == 0) ? 1 : fact(n - 1) * n;
}

// Implementing a suitable version of the modern Fisher-Yates shuffle
//  for i from 0 to n−1 do
//     j ← random integer such that i ≤ j < n
//     exchange a[i] and a[j]
//  Src: https://en.wikipedia.org/wiki/Fisher-Yates_shuffle

// returning array with random shuffe except in the first position
//      [1,2,3,...,n] --> [1,r2,r3,...,rn]
void shuffle(const int n, int *a){
  int j;
  real tmp;
  for(int i = 1; i<n; i++){
    j = getRand(i,n);
    tmp = a[j];
    a[j] = a[i];
    a[i] = tmp;
  }
}

void full_shuffle(const int n, int *a){
  int j;
  real tmp;
  for(int i = 0; i<n; i++){
    j = getRand(i,n);
    tmp = a[j];
    a[j] = a[i];
    a[i] = tmp;
  }
}

__global__ void setx(real *x, const real set){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  x[tid] = set;
}

__global__ void set_min_max(real *arr, const real n_rans){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  const int loops = n_rans / d_nev;
  for(int i = 0; i<loops; i++){
    if(arr[tid + i*d_nev] >= 0.99999){
      arr[tid+i*d_nev ] = 0.99999;
    }
    if(arr[tid + i*d_nev] <= 0.00001){
      arr[tid+i*d_nev ] = 0.000001;
    }
  }
}

__global__ void add_square(real *array, const real *toadd){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  array[tid] += toadd[tid] * toadd[tid];
}

__global__ void fill_renormalization_scale(real *ret_r_scale){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  const real mz = 91.188;
  const real mz2 = mz*mz;
  ret_r_scale[tid] = mz2;
}

__global__ void fill_factorization_scale(real *ret_f_scale){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  const real mz = 91.188;
  const real mz2 = mz*mz;
  ret_f_scale[tid] = mz2;
}

void printsummary(std::string name, float t_name, float t_tot, int nev){
  std::cout << "   " << name << "  \t " << t_name << "  \t\t " << t_name / (1000*nev) << "\t\t "
	    << int( 100*t_name/(t_tot)) << std::endl;
}


#endif

#include "config.h"
#include "tools.h"
#include <iostream>
#include <stdexcept>
#include <vector>

class ColorIntegrator{
  int n;
  int NC;
  bool found = false;
  
 public:
  int *shuf;
  int *colors;
  int *perms;
  int *ids;
  int *allperms;
  
  int n_perms = 0;
  int nev;
  int maxperms;
  //std::vector<std::vector<int>> perms;
  //std::vector<int> perms;
  ColorIntegrator(const int _n, const int _NC);
  ColorIntegrator(const int _n, const int _NC, const int nev, const int maxperms);
  ~ColorIntegrator();
  void UniformColors();
  void Colors();
  void NewColors();
  void ColorSumStart();
  void ColorSumNext();
  bool trivialcheck();
  bool findperms();
  bool findperms(std::vector<int> perm, std::vector<int> inds);
  real UniformWeight();
  real Weight();
  real NewWeight();
  void ConcatPerms();
};
  
ColorIntegrator::ColorIntegrator(const int _n, const int _NC){
  n  = _n;
  NC = _NC;
  if(_NC != 3){
    throw std::invalid_argument("At the moment the Color Integrator is hardcoded for N_C = 3!");
  }
  colors = (int *)malloc(sizeof(int)*2*n);
  shuf   = (int *)malloc(sizeof(int)*n);
  perms  = (int *)malloc(sizeof(int)*n*fact(n-1));
  for(int i = 0; i<n; i++){
    shuf[i] = i;
  }
}

ColorIntegrator::ColorIntegrator(const int _n, const int _NC, const int _nev, const int _maxperms){
  n  = _n;
  NC = _NC;
  nev = _nev;
  maxperms = _maxperms;
  
  if(_NC != 3){
    throw std::invalid_argument("At the moment the Color Integrator is hardcoded for N_C = 3!");
  }
  colors = (int *)malloc(sizeof(int)*2*n);
  shuf   = (int *)malloc(sizeof(int)*n);
  ids    = (int *)malloc(sizeof(int)*nev);
  perms  = (int *)malloc(sizeof(int)*n*fact(n-1));
  allperms = (int *)malloc(sizeof(int)*n*nev*maxperms);
  for(int i = 0; i<n; i++){
    shuf[i] = i;
  }
    
}

ColorIntegrator::~ColorIntegrator(){
  std::free(perms);
  std::free(colors);
  std::free(shuf);
  std::free(allperms);
  std::free(ids);
}


void ColorIntegrator::ColorSumStart(){
  for(int i = 0; i<2*n; i++){
    colors[i] = 0;
  }
}

void ColorIntegrator::ColorSumNext(){
  for(int i = 2*(n-1)+1; i>=0; i--){
    if(colors[i] < NC-1){
      colors[i]++;
      break;
    }
    else{
      colors[i] = 0;
    }
  }
}

void ColorIntegrator::UniformColors(){
  for(int i = 0; i<2*n; i++){
    colors[i] = getRand(0,NC);
  }
}

// Dice colors as described in 0808.3674.
// This methods leads to matching color and anti color numbers
// but not always to valid colors flows
void ColorIntegrator::Colors(){
  for(int i = 0; i<n; i++){
    colors[2*i] = getRand(0,NC);
  }
  full_shuffle(n, shuf);
  for(int i = 0; i<n; i++){
    colors[2*i+1] = colors[2*shuf[i]];
  }
}

void ColorIntegrator::NewColors(){
  // Color Sampling a la mk
  // Goal is to also always find at least one permutation
  //    which would be preferably on the gpu since we would
  //    not generate events whose corresponding matrix element
  //    we do not evaluate once
  full_shuffle(n, shuf);
  colors[2*shuf[0]] = getRand(0,NC);
  for(int i = 0; i<n; i++){
    colors[2*shuf[i]+1] = colors[2*shuf[i-1]];
    colors[2*shuf[i]  ] = getRand(0,NC);
  }
  colors[2*shuf[0]+1] = colors[2*shuf[n-1]];
}

real ColorIntegrator::UniformWeight(){
  return pow(NC, 2*n);
}

real ColorIntegrator::Weight(){
  int sum[3] = {0,0,0};
  for(int i = 0; i<n; i++){
    sum[colors[2*i]] ++;
  }
  real w = 1.0;
  w *= pow(NC,n);
  w *= fact(n);
  for(int i = 0; i<NC; i++){
    w /= fact(sum[i]);
  }
  return w;
}

real ColorIntegrator::NewWeight(){
  return 1.;
}

bool ColorIntegrator::trivialcheck(){
  int sum[2*3] = {0,0,0,0,0,0};
  for(int i = 0; i<n; i++){
    sum[2*colors[2*i]] ++;
    sum[2*colors[2*i+1]+1] ++;
  }
  for(int i = 0; i<NC; i++){
    if(sum[2*i] != sum[2*i+1]){
      found = false;
      return false;
    }
  }
  return true;
}

bool ColorIntegrator::findperms(std::vector<int> perm, std::vector<int> inds){
  // found full permutation
  if(perm.size() == n){
    for(int i = 0; i<n; i++){
      perms[n_perms*n + i] = perm[i];
      //perms.push_back(perm[i]);
    }
    n_perms++;
    //perms.push_back(perm);
    found = true;
    return true;
  }
  
  // recursion
  for(size_t i = 0; i<inds.size(); i++){
    int ind = inds[i];
    if(colors[2*ind+1] == colors[2*perm.back()]){
      std::vector<int> linds(inds);
      std::vector<int> lperm(perm); 
      linds.erase(linds.begin() + i);
      lperm.push_back(ind);
      findperms(lperm, linds);
    }
  }
  return false;
}

bool ColorIntegrator::findperms(){
  found = false;
  n_perms = 0;
  //perms.clear();
  //for(int i = 0; i<fact(n-1)
  if(!trivialcheck()){
    return found;
  }

  std::vector<int> perm;
  std::vector<int> inds;
  for(int i = 1; i<n; i++){
    inds.push_back(i);
  }
  perm.push_back(0);
  findperms(perm, inds);
  return found;
}

void ColorIntegrator::ConcatPerms(){
  int currentId = 0;
  bool foundperms;
  for(int i = 0; i<nev; i++){
    ids[i] = -1;
  }
  for(int i = 0; i<nev; i++){
    Colors();
    foundperms = findperms();
    if(foundperms){
      // Note Id of color config
      for(int j = 0; j<n_perms; j++){
	if(currentId > nev*maxperms){ break;}
	ids[currentId + j] = i;
	//std::cout << "perm = ";
	for(int k = 0; k<n; k++){
	  allperms[currentId + j + k*nev] = perms[j*n + k];
	  //std::cout << perms[j*n+k] << " ";
	}
	//std::cout << std::endl;
      }
      currentId += n_perms;
    }
  }
}

















//
//
//

#ifndef SHUFFLEH
#define SHUFFLEH

#include "config.h"

// Implementing a suitable version of the modern Fisher-Yates shuffle
//  for i from 0 to n−1 do
//     j ← random integer such that i ≤ j < n
//     exchange a[i] and a[j]
//  Src: https://en.wikipedia.org/wiki/Fisher-Yates_shuffle

// returning array with random shuffe except in the first position
//      [1,2,3,...,n] --> [1,r2,r3,...,rn]
__global__ void shuffle(real *rans, int *shuf){
  int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  for(int i = 0; i<n; i++){    
    shuf[tid + i*d_nev] = i;
  }
  int j;
  real tmp;
  for(int i = 0; i<n-1; i++){
    j = i + rans[tid + i * d_nev] * ((n-1)-i);
    tmp = shuf[tid + j*d_nev];
    shuf[tid + j*d_nev] = shuf[tid + i*d_nev];
    shuf[tid + i*d_nev] = tmp;
  }
}

#endif

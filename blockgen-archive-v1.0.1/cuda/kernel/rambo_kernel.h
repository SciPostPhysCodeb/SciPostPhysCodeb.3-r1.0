//
// Rambo Kernel for the generation of events on the GPU
//

#include "config.h"

__global__ void generatePoint(real *p, real *wgt, real *rans, real *xx1,
			      real *xx2, int gammaprod){
  // unique id of the thread, for d_nev events, tid ranges from [0,d_nev)
  // Thus we set up one thread for each event
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);

  /*
  const real x1 = xx1[tid];
  const real x2 = xx2[tid];
  if(tid == 0){
    printf("x1 = %f, x2 = %f\n", x1, x2);
  }
  */
  const real x1 = 1;
  const real x2 = 1;

  
  // For optimal memory acess, consecutive threads should read data stored
  // consecutive in memory. For that reason we increment by the total length
  // --> d_nev
  //
  // First two 4-momenta can be set directly
  p[tid + 0 * d_nev] = x1 * -ecm/2.;
  p[tid + 1 * d_nev] = 0;
  p[tid + 2 * d_nev] = 0;
  p[tid + 3 * d_nev] = x1 * -ecm/2.;
  p[tid + 4 * d_nev] = x2 * -ecm/2.;
  p[tid + 5 * d_nev] = 0;
  p[tid + 6 * d_nev] = 0;
  p[tid + 7 * d_nev] = x2 * ecm/2.;

  const real w = sqrt(x1*x2) * ecm;

  // Apply trafo (3.1) in Computer Physics Communications 40 (1986) 359—373
  real c, phi,q;
  for(int i = nin; i<nin + nout; i++){
    c = 2*rans[tid+(4*(i-2)+0)*d_nev] -1 ;
    phi = 2*M_PI*rans[tid+(4*(i-2)+1)*d_nev];
    q = -log(rans[tid+(4*(i-2)+2)*d_nev]*rans[tid+(4*(i-2)+3)*d_nev]);
    p[tid + (4*i+0)*d_nev] = q;
    p[tid + (4*i+1)*d_nev] = q * sqrt(1-c*c) * cos(phi);
    p[tid + (4*i+2)*d_nev] = q * sqrt(1-c*c) * sin(phi);
    p[tid + (4*i+3)*d_nev] = q * c;
  }

  // Apply trafo (2.5) in Computer Physics Communications 40 (1986) 359—373
  real Q[4] = {0,0,0,0};
  for (int i=nin; i<nin+nout; i++){
    for (int j=0; j<4; j++){
      Q[j] += p[tid + (4*i+j)*d_nev];
    }
  }

  const real M = sqrt(Q[0]*Q[0] - Q[1]*Q[1] - Q[2]*Q[2] - Q[3]*Q[3]);
  real gamma = Q[0] / M;
  const real a = 1./(1+gamma);
  const real X = w/M;
  const real b[4] = {-Q[0]/M, -Q[1]/M, -Q[2]/M, -Q[3]/M};

  // Apply trafo (2.4) in Computer Physics Communications 40 (1986) 359—373
  real bq, q0;
  for(int i = nin; i<nin+nout; i++){
    bq = b[1]*p[tid + (4*i+1)*d_nev]
      + b[2]*p[tid + (4*i+2)*d_nev]
      + b[3]*p[tid + (4*i+3)*d_nev];
    q0 = p[tid + (4*i+0)*d_nev];
    p[tid + (4*i+0)*d_nev] = X * (gamma * q0 + bq);
    p[tid + (4*i+1)*d_nev] = X * (p[tid + (4*i+1)*d_nev] + b[1]*q0 + a*bq*b[1]);
    p[tid + (4*i+2)*d_nev] = X * (p[tid + (4*i+2)*d_nev] + b[2]*q0 + a*bq*b[2]);
    p[tid + (4*i+3)*d_nev] = X * (p[tid + (4*i+3)*d_nev] + b[3]*q0 + a*bq*b[3]);
  }

  // boost the momenta in the respective rest frame
  // Lorentzboost
  const real beta = -(x1-x2)/(x1+x2);
  gamma = sqrt(1. / (1.-beta*beta));
  real e,pz;
  for(int i = nin; i<nout+nin; i++){
    e  = p[tid + (4*i+0)*d_nev];
    pz = p[tid + (4*i+3)*d_nev];
    p[tid + (4*i+0)*d_nev] = gamma*(e-beta*pz);
    p[tid + (4*i+3)*d_nev] = gamma*(pz-beta*e);
  }

  // Fill weights
  wgt[tid] = pow(2*M_PI, 4-3*nout) *  pow(M_PI / 2., nout-1) * pow(w, 2*nout-4) / gammaprod;
}


//
// Some test events with the refernce value of average |A|^2
// Generated with the python cobg version, which was verified agains
// SH original Cobg Version
//

__global__ void generateTestPointn4(real *p, real *wgt, const int gammaprod){
  // test event for n = 4
  
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);

  real arr[16] = {-7000.0000,     0.0000,     0.0000, -7000.0000, -7000.0000,     0.0000,
    0.0000,  7000.0000,  7000.0000,  2112.2036,  -105.5448, -6672.8896,
    7000.0000, -2112.2036,   105.5448,  6672.8896};

  for(int i = 0; i<16; i++){
    p[tid + i*d_nev] = arr[i];
  }

  const real w = ecm;
  wgt[tid] = pow(2*M_PI, 4-3*nout) *  pow(M_PI / 2., nout-1) * pow(w, 2*nout-4) / gammaprod;
}

__global__ void generateTestPointn5(real *p, real *rans){
  // test event for n = 5
  
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  // Reference cobg yields
  //   |A|^2 = 0.0023236674044286873 +/- 3.0678778234903387e-05
  //   for N = 1e4 
  p[tid +  0  * d_nev] =  -6500.0 ;
  p[tid +  1  * d_nev] =  0 ;
  p[tid +  2  * d_nev] =  0 ;
  p[tid +  3  * d_nev] =  -6500.0 ;
  p[tid +  4  * d_nev] =  -6500.0 ;
  p[tid +  5  * d_nev] =  0 ;
  p[tid +  6  * d_nev] =  0 ;
  p[tid +  7  * d_nev] =  6500.0 ;
  p[tid +  8  * d_nev] =  6479.658037734357 ;
  p[tid +  9  * d_nev] =  -4497.761196233858 ;
  p[tid +  10  * d_nev] =  4662.517290277141 ;
  p[tid +  11  * d_nev] =  -130.55659881900849 ;
  p[tid +  12  * d_nev] =  6479.240264818215 ;
  p[tid +  13  * d_nev] =  4507.706735598351 ;
  p[tid +  14  * d_nev] =  -4651.0915644068855 ;
  p[tid +  15  * d_nev] =  168.7650869878248 ;
  p[tid +  16  * d_nev] =  41.10169744742123 ;
  p[tid +  17  * d_nev] =  -9.945539364492456 ;
  p[tid +  18  * d_nev] =  -11.425725870255272 ;
  p[tid +  19  * d_nev] =  -38.20848816881484 ;
}

__global__ void generateTestPointn6(real *p, real *rans){
  // test event for n = 6
  
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  // Reference cobg yields
  //   |A|^2 = 8.75544e-13 +/- 1.206e-15
  //   for N = ???
  p[tid +  0  * d_nev] =  -6500.0 ;
  p[tid +  1  * d_nev] =  0 ;
  p[tid +  2  * d_nev] =  0 ;
  p[tid +  3  * d_nev] =  -6500.0 ;
  p[tid +  4  * d_nev] =  -6500.0 ;
  p[tid +  5  * d_nev] =  0 ;
  p[tid +  6  * d_nev] =  0 ;
  p[tid +  7  * d_nev] =  6500.0 ;
  p[tid +  8  * d_nev] =  2233.7660598022044 ;
  p[tid +  9  * d_nev] =  -967.3482326419581 ;
  p[tid +  10  * d_nev] =  1500.4731771191193 ;
  p[tid +  11  * d_nev] =  -1342.582754050863 ;
  p[tid +  12  * d_nev] =  4237.497613127381 ;
  p[tid +  13  * d_nev] =  4014.392135945505 ;
  p[tid +  14  * d_nev] =  532.0700367944247 ;
  p[tid +  15  * d_nev] =  1248.1759796057308 ;
  p[tid +  16  * d_nev] =  4950.765215231869 ;
  p[tid +  17  * d_nev] =  -4504.258354582837 ;
  p[tid +  18  * d_nev] =  -1934.7009949803446 ;
  p[tid +  19  * d_nev] =  691.8561639117177 ;
  p[tid +  20  * d_nev] =  1577.971111838544 ;
  p[tid +  21  * d_nev] =  1457.2144512792906 ;
  p[tid +  22  * d_nev] =  -97.84221893319969 ;
  p[tid +  23  * d_nev] =  -597.4493894665845 ;
}

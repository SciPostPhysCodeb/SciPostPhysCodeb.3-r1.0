#pragma once
#include "config.h"

__global__ void MonteCarloLeadingColor(real *ret_mc,
				       const real *x1,
				       const real *x2,
				       const real *me,
				       const real *psweight,
				       const bool *cuts){
  const int tid = threadIdx.x + blockDim.x*blockIdx.x;
  const int NC = 3;
  const real conversion = 0.389379*1e9;

  real dcs = 1.0;
  dcs /= 2 * ecm*ecm;
  dcs *= n-1;
  dcs *= me[tid];
  dcs *= pow(4 * M_PI*0.118, n-2);
  dcs *= psweight[tid];
  dcs *= conversion;
  dcs *= pow(NC, n-2) * (NC*NC-1);
  dcs /= 64.;
  dcs *= (int) cuts[tid];
  ret_mc[tid] = dcs;
}

__global__ void MonteCarloColorSummed(real *ret_mc,
				      const real *x1,
				      const real *x2,
				      const real *me,
				      const real *psweight,
				      const bool *cuts,
				      const int symmetry_factor){
  const int tid = threadIdx.x + blockDim.x*blockIdx.x;
  const real conversion = 0.389379*1e9;
  
  real dcs = 1.0;
  dcs /= 2 * ecm*ecm;
  dcs *= me[tid];
  dcs /= symmetry_factor;
  dcs *= pow(4 * M_PI *0.118, n-2);
  dcs *= psweight[tid];
  dcs *= conversion;
  dcs /= 64.;
  dcs *= (int) cuts[tid];
  ret_mc[tid] = dcs;
}

__global__ void MonteCarloColorSampled(real *ret_mc,
				      const real *x1,
				      const real *x2,
				      const real *me,
				      const real *psweight,
				      const bool *cuts,
				      const int symmetry_factor){
  const int tid = threadIdx.x + blockDim.x*blockIdx.x;
  const real conversion = 0.389379*1e9;
  
  real dcs = 1.0;
  dcs /= 2 * ecm*ecm;
  dcs *= pow(me[tid],2);
  dcs *= pow(3,2*n);
  dcs /= symmetry_factor;
  dcs *= pow(4 * M_PI *0.118 ,n-2);
  dcs *= psweight[tid];
  dcs *= conversion;
  dcs /= 64.;
  dcs *= (int) cuts[tid];
  ret_mc[tid] = dcs;
}




#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include <curand.h>
#include <string>
#include <fstream>
#include <chrono>
#include <thrust/sort.h>
#include <thrust/sequence.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/count.h>
#include <thrust/execution_policy.h>

// Global constants avaiable on the device
__constant__ int nin;
__constant__ int nout;
__constant__ real ecm;
__constant__ int d_nev;
__constant__ int NN; // size of recursion data
__constant__ int n; // size of recursion data

#include "../tools/helper_cuda.h"
#include "../tools/vec4.h"

// Include kernel functions
#include "../kernel/rambo_kernel.h"
#include "../kernel/cut_kernel.h"
#include "../kernel/shuffle_kernel.h"
#include "../kernel/monte_carlo_kernel.h"

// CPU functionality
#include "../tools/color.h"
#include "../tools/tools.h"
#include "../tools/permutations.h"

// local kernel
#include "cobg_kernel.h"

struct is_zero
{
  __host__ __device__
  bool operator()(const int x)
  {
    return (x == 0);
  }
};

struct is_valid
{
  __host__ __device__
  bool operator()(const int x)
  {
    return x < 2e7;
  }
};

__global__ void diceColors(real *rans, int *colors, int *perm){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  for(int i = 0; i<n; ++i){
    colors[tid + 2*i*d_nev] = static_cast<int>(rans[tid+i*d_nev] * 3);
  }
  for(int i = 0; i<n; ++i){
    colors[tid + (2*i+1)*d_nev] = static_cast<int>(rans[tid+ perm[tid+i*d_nev] *d_nev] * 3);
  }
}

__global__ void dprint(int *key, int *indices, int n_valid){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  for(int i = 0; i<n_valid; i++){
    printf("tid = %d, ind = %d, key = %d\n", tid, indices[i], key[i]);
  }
}

__global__ void findPerms(int *colors, int *perms, int *valids, int *key, int nperms){
  const int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  int counter = 0;
  valids[tid] = 0;
  
  for(int j = 0; j<nperms; ++j){
    int valid = 0;    
    valid += (colors[tid + (perms[j*n + 0]+1) *d_nev]
	      == colors[tid + (2* perms[j*n + (n-1)]) *d_nev]);
    for(int i = 0; i<n-1; ++i){
      valid += (colors[tid + (2*perms[j*n + i])*d_nev]
		== colors[tid + (2*perms[j*n + (i+1)]+1)*d_nev]);
    }
    if(valid == n){
      ++valids[tid];
      valids[tid + counter*d_nev] = j;
      ++counter;
    }
  }
  
  if(counter > 0){
    key[tid] = counter;
  }
  else{
    key[tid] = 2e7;
  }
}
  

// Main Programms
//
int main(int argc, const char **argv){
  real *h_moms, *d_moms, *d_wgts, *h_AA, *d_AA, *d_y, *d_phi, *d_AA_tmp;
  real *d_rans_x1, *d_rans_x2, *d_rans_rambo, *d_rans_cobg, *d_rans_shuf, *d_rans_color;
  bool *h_cuts, *d_cuts;
  real *d_r_scale, *d_f_scale;
  real *d_mc_results;
  vec4 *d_Jv, *d_Pv;
  
  int *d_shuffle;
  real sum = 0;
  real sum2 = 0;
  real max = 0;
  real accepted = 0;
  real dcs;

  // Global Variables avaiable on device and host
  const int h_nin = 2;
  int h_nout = 2;
  if (argc > 1){
    h_nout = atoi(argv[1]);
  }
  int nblocks = 100;
  int blocksize = 512;  
  if (argc > 3){
    nblocks = atoi(argv[3]);
  }
  if (argc > 4){
    blocksize = atoi(argv[4]);
  }
  
  const real h_ecm = 14000;
  const int nev = blocksize*nblocks;
  const int h_nn = (h_nout+h_nin-1)*(h_nout+h_nin)/2.;
  const int h_n = h_nin + h_nout;
  const int gammaprod = fact(h_nout-1) * fact(h_nout-2);
  const int symm_fact = fact(h_n-2);
  const int NC = 3;
  
  // initialise card
  //findCudaDevice(argc, argv);
  
  // Transfer variables to device
  checkCudaErrors( cudaMemcpyToSymbol(nin,   &h_nin,   sizeof(h_nin)) );
  checkCudaErrors( cudaMemcpyToSymbol(nout,  &h_nout,  sizeof(h_nout)) );
  checkCudaErrors( cudaMemcpyToSymbol(ecm,   &h_ecm,   sizeof(h_ecm)) );
  checkCudaErrors( cudaMemcpyToSymbol(d_nev, &nev,     sizeof(nev)) );
  checkCudaErrors( cudaMemcpyToSymbol(NN,    &h_nn,    sizeof(h_nn)) );
  checkCudaErrors( cudaMemcpyToSymbol(n,     &h_n,     sizeof(h_n)) );

  // allocate memory for the monte carlo results
  checkCudaErrors(cudaMalloc((void **)&d_mc_results, sizeof(real)*nev));

  // initialise CUDA timing
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  // allocate memory on host and device for the momenta
  h_moms = (real *)malloc(sizeof(real)*nev*4*(h_nin + h_nout));
  checkCudaErrors( cudaMalloc((void **)&d_moms, sizeof(real)*nev*4*(h_nin + h_nout)) );
  checkCudaErrors( cudaMalloc((void **)&d_wgts, sizeof(real)*nev) );

  // malloc space for the random numbers
  //
  // probably to combine all these into one array of combined lenght
  //    if random nuber generation ever becomes significant this should
  //    be the starting point
  //
  const int n_rans_xx    = nev;
  const int n_rans_rambo = 4*h_nout*nev;
  const int n_rans_cobg  = h_n*nev;
  const int n_rans_shuf  = (h_n)*nev;
  const int n_rans_color = h_n*nev;
  checkCudaErrors( cudaMalloc((void **)&d_rans_x1,    sizeof(real)*n_rans_xx) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_x2,    sizeof(real)*n_rans_xx) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_rambo, sizeof(real)*n_rans_rambo) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_cobg,  sizeof(real)*n_rans_cobg) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_shuf,  sizeof(real)*n_rans_shuf) );
  checkCudaErrors( cudaMalloc((void **)&d_rans_color, sizeof(real)*n_rans_color) );

  // alocate the squared amplitudes in the host and device
  // probably at some point we could also sum the squared amplitudes on device
  // but for debugging this should be the better version
  h_AA = (real *)malloc(sizeof(real)*nev);
  checkCudaErrors( cudaMalloc((void **)&d_AA,   sizeof(real)*nev) );
  checkCudaErrors( cudaMalloc((void **)&d_AA_tmp, sizeof(real)*nev) );

  // Allocate memory on device for the Jcurrents and corresponding momenta
  checkCudaErrors( cudaMalloc((void **)&d_Jv,       sizeof(vec4)*nev*(h_nn+1) ));
  checkCudaErrors( cudaMalloc((void **)&d_Pv,       sizeof(vec4)*nev*(h_nn+1) ));
  checkCudaErrors( cudaMalloc((void **)&d_shuffle, sizeof(int)*nev*h_n ));
  checkCudaErrors( cudaMalloc((void **)&d_r_scale, sizeof(real)*nev ));
  checkCudaErrors( cudaMalloc((void **)&d_f_scale, sizeof(real)*nev ));

  // Allocate memory on host and device for the cuts
  h_cuts = (bool *)malloc(sizeof(bool)*nev);
  checkCudaErrors( cudaMalloc((void **)&d_cuts, sizeof(bool)*nev) );
  checkCudaErrors( cudaMalloc((void **)&d_y,    sizeof(real)*nev*h_n) );
  checkCudaErrors( cudaMalloc((void **)&d_phi,  sizeof(real)*nev*h_n) );

  // initialise random number generator
  curandGenerator_t gen;
  checkCudaErrors( curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT) );
  checkCudaErrors( curandSetPseudoRandomGeneratorSeed(gen, 0) );

  // Color stuff
  // alocate array large enough to hold the maximal number of permutations
  //    storage wise, this might not be optimal
  const int n_perms_max = fact(h_n-1)*h_n;
  int *d_color_perms, *d_colors;
  checkCudaErrors( cudaMalloc((void **)&d_color_perms, sizeof(int)*n_perms_max) );
  checkCudaErrors( cudaMalloc((void **)&d_colors,      sizeof(int)*nev*h_n*2 ));

  real *d_polarisation_vectors;
  checkCudaErrors( cudaMalloc((void **)&d_polarisation_vectors, sizeof(real)*nev*h_n*2*4));

  
  // Generate and copy the permutations in lexicographical order
  int  *d_perms;
  int  *d_valid;
  int  *d_key;
  Permutations Perms(h_n, 1);
  Perms.createPerms();
  //const int n_perms_max = fact(h_n-2)*h_n;
  checkCudaErrors(cudaMalloc((void **)&d_perms,        sizeof(int)*Perms.n_perms*h_n) );
  checkCudaErrors(cudaMemcpy(d_perms, Perms.perms,     sizeof(int)*Perms.n_perms*h_n,
			     cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMalloc((void **)&d_valid,        sizeof(int)*(Perms.n_perms*nev+1)) );
  checkCudaErrors(cudaMalloc((void **)&d_key,          sizeof(int)*nev) );  

  
  int sweeps = 1;
  if (argc > 2){
    sweeps = atoi(argv[2]);
  }
  const int sharedMemory = (h_n+1)*blocksize;

  float t_rans, t_cuts, t_rambo, t_cobg, t_mc;
  float t_perms, t_sort;
  bool print_progress = sweeps >= 10;
  print_progress = true;
  auto t0 = std::chrono::high_resolution_clock::now();
  if(print_progress){
    std::cout << "\nMONTE-CARLO PROGRESS:" << std::endl;
  }
  //
  // MONTE CARLO STARTS HERE
  //
  for(int j = 1; j<=sweeps; j++){    
    // generate all random numbers
    cudaEventRecord(start);
#ifdef USE_DOUBLE_PRECISION
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_x1,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_x2,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_rambo, n_rans_rambo) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_cobg,  n_rans_cobg) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_shuf,  n_rans_shuf) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_color, n_rans_color) );
#else
    checkCudaErrors( curandGenerateUniform(gen, d_rans_x1,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_x2,    n_rans_xx) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_rambo, n_rans_rambo) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_cobg,  n_rans_cobg) );
    checkCudaErrors( curandGenerateUniform(gen, d_rans_shuf,  n_rans_shuf) );
    checkCudaErrors( curandGenerateUniformDouble(gen, d_rans_color, n_rans_color) );
    set_min_max<<<nblocks, blocksize>>>(d_rans_x1,    n_rans_xx);
    set_min_max<<<nblocks, blocksize>>>(d_rans_x2,    n_rans_xx);
    set_min_max<<<nblocks, blocksize>>>(d_rans_rambo, n_rans_rambo);
    set_min_max<<<nblocks, blocksize>>>(d_rans_cobg,  n_rans_cobg);
    set_min_max<<<nblocks, blocksize>>>(d_rans_shuf,  n_rans_shuf);
    set_min_max<<<nblocks, blocksize>>>(d_rans_color, n_rans_color);
#endif
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_rans, start, stop);

    
    // generate nev events using the rambo kernel
    cudaEventRecord(start);
    generatePoint<<<nblocks, blocksize>>>(d_moms, d_wgts, d_rans_rambo,
					  d_rans_x1, d_rans_x2, gammaprod);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    checkCudaErrors(cudaEventElapsedTime(&t_rambo, start, stop));
    
    // check cuts and flag if outside cuts
    cudaEventRecord(start);
    cuts<<<nblocks, blocksize>>>(d_cuts, d_moms, d_y, d_phi, 20., 2.5, 0.4);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    checkCudaErrors(cudaEventElapsedTime(&t_cuts, start, stop));

    cudaEventRecord(start);
    shuffle<<<nblocks, blocksize>>>(d_rans_shuf, d_shuffle);
    diceColors<<<nblocks, blocksize>>>(d_rans_color, d_colors, d_shuffle);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_mc, start, stop);

    cudaEventRecord(start);
    findPerms<<<nblocks, blocksize>>>(d_colors, d_perms, d_valid, d_key, Perms.n_perms);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_perms, start, stop);

    // new color flor stuff
    cudaEventRecord(start);
    thrust::device_vector<int>  indices(nev); 
    thrust::sequence(indices.begin(),indices.end());
    thrust::device_ptr<int> t_a = thrust::device_pointer_cast(d_key);
    thrust::sort_by_key(thrust::device, t_a, t_a+nev, indices.begin());
    int result = thrust::count_if(thrust::device, t_a, t_a+nev, is_valid());    
    int *ptrDVec = thrust::raw_pointer_cast(&t_a[0]);
    int *dv_ptr  = thrust::raw_pointer_cast(indices.data());
    
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_sort, start, stop);    

    // Find out how many blocks & threads to start
    cudaEventRecord(start);
    cudaMemset(d_AA, 0, nev*sizeof(real));
    fill_polarisation_vectors<<<nblocks, blocksize>>>(d_polarisation_vectors, d_moms);
    cobg<<<result/blocksize + 1, blocksize, sharedMemory*sizeof(vec4)>>>(d_Jv, d_Pv, d_moms, d_rans_cobg, d_AA, ptrDVec, d_valid, d_perms, result, dv_ptr, d_polarisation_vectors);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    cudaEventElapsedTime(&t_cobg, start, stop);
    
    // combine all of the above
    cudaEventRecord(start);
    MonteCarloColorSampled<<<nblocks, blocksize>>>(d_mc_results, d_rans_x1, d_rans_x2, d_AA, d_wgts, d_cuts, symm_fact);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    
    // copy back results to the host
    checkCudaErrors(cudaMemcpy(h_AA, d_mc_results, sizeof(real)*nev, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(h_cuts, d_cuts,     sizeof(bool)*nev, cudaMemcpyDeviceToHost));
    real tmp  = 0;
    real tmp2 = 0;
    for(int i = 0; i<nev; i++){
      if (h_cuts[i]){
	if (h_AA[i] > max){
	  max = h_AA[i];
	}
	dcs = h_AA[i];
	tmp += dcs;
	tmp2 += dcs*dcs;
	accepted += h_cuts[i];
      }
    }
    sum += tmp / (1.*nev);
    sum2 += tmp2 / (1.*nev);        
  } // end of sweep for loop
  
  // Summary + Final computations
  printf("\n\nPROCESS:\n");
  printf("   Number of particles : %d\n", h_nin+h_nout);

  printf("\nCOMPUTATIONAL DETAILS:\n");
  printf("   Number of blocks    : %d\n", nblocks);
  printf("   Threads / block     : %d\n", blocksize);
  printf("   Total No. threds    : %d\n", nblocks * blocksize);
  printf("   Total No. sweeps    : %d\n", sweeps);
  printf("   Number of events    : %.2g\n",1.*nev*sweeps);
  
  printf("\nPERFORMANCE SUMMARY:\n");
  
  float t_Tot = t_rans + t_cuts + t_rambo + t_cobg + t_mc + t_sort + t_perms;
  printf("   Process \t Total Time [ms] \t Time/event [s] \t Frac of Total Time [%]\n");
  printf("   ----------------------------------------------------------------------------------- \n");
  printsummary("Rans  ", t_rans,    t_Tot, nev);
  printsummary("Rambo ", t_rambo,   t_Tot, nev);
  printsummary("Cuts  ", t_cuts,    t_Tot, nev);
  printsummary("Cobg  ", t_cobg,    t_Tot, nev);
  printsummary("Perms ", t_perms,   t_Tot, nev);
  printsummary("Sort  ", t_sort,    t_Tot, nev);
  printsummary("MC    ", t_mc,      t_Tot, nev);
  printsummary("Total ", t_Tot,     t_Tot, nev);

  real avg = sum / sweeps;
  real stddev = sqrt(sum2 / sweeps - pow(sum / sweeps, 2)) / sqrt(1.*nev * sweeps);  
  printf("\nRESULTS:\n");
  printf("   |A|^2 = %.4e +/- (%.2e = %.3g %) \n", avg, stddev, stddev/avg*100);
  printf("   Max weight = %.4e\n", max);
  printf("   Sampling efficiency = %.3g %\n", 100 * accepted * 1. / (1. * sweeps * nev));

  // CUDA exit 
  // also free memory
  cudaFree(d_wgts);
  cudaFree(d_rans_x1);
  cudaFree(d_rans_x2);
  cudaFree(d_rans_rambo);
  cudaFree(d_rans_cobg);
  cudaFree(d_AA);
  cudaFree(d_cuts);
  cudaFree(d_y);
  cudaFree(d_phi);
  free(h_moms);
  free(h_AA);
  free(h_cuts);
  cudaDeviceReset();
}

#include "BlockGen/Rambo.cuh"

using namespace BlockGen;

Rambo::Rambo(int nin_, int nout_, double ptCut_, double etaCut_, double deltaRCut_) 
        : nin(nin_), nout(nout_), ptCut(ptCut_), etaCut(etaCut_), deltaRCut(deltaRCut_) {
    double pi2log = log(M_PI/2.);
    double *Z = new double[nout+1];
    Z[2] = pi2log;
    for(size_t k=3; k<=nout; ++k) 
        Z[k] = Z[k-1]+pi2log-2.*log(double(k-2));
    for(size_t k=3; k<=nout; ++k) 
        Z[k] = Z[k]-log(double(k-1));
    Z_N = Z[nout];
    delete[] Z;

    Vec4D k(1.0, 1.0, 0.0, 0.0);
    m_kp = SpinorD::ConstructSpinor(1, k);
    m_km = SpinorD::ConstructSpinor(-1, k);
}

__device__
size_t Rambo::Cut(const Event &event, const int d_nev) const {
    int pass = 1;
#ifdef NDEBUG
    if(threadIdx.x==0 && blockIdx.x==0) {
        printf("ptCut = %f, etaCut = %f\n", ptCut, etaCut);
    }
#endif
    for(int i = nin; i < nin+nout; ++i) {
#ifdef NDEBUG
        if(threadIdx.x==0 && blockIdx.x==0) {
	    printf("event.mom[Cur(%d)].PT() = %f\n", i, event.mom[Cur(i)].PT());
        }
#endif
        pass *= event.mom[Cur(i)*d_nev].PT() > ptCut;
        pass *= abs(event.mom[Cur(i)*d_nev].Eta()) < etaCut;
    }
    for(int i = nin; i < nin+nout-1; ++i) {
        for(int j = i+1; j < nin+nout; ++j) {
	    pass *= event.mom[Cur(i)*d_nev].DeltaR(event.mom[Cur(j)*d_nev]) > deltaRCut;
#ifdef NDEBUG
            if(threadIdx.x==0 && blockIdx.x==0)
                printf("DeltaR(%d, %d) = %f\n", i, j, event.mom[Cur(i)].DeltaR(event.mom[Cur(j)]));
#endif
        }
    }
#ifdef NDEBUG
    if(threadIdx.x==0 && blockIdx.x==0) {
        printf("Cut = %d\n", pass);
    }
#endif
    return pass;
}

#ifdef TESTING
__device__
void swap(int &a, int &b) {
    int tmp = a;
    a = b;
    b = tmp;
}

__device__
void Permute(int n, int m_n, int *p_st, int *p_per)
{
  for(int i=0;i<m_n;++i) {
    p_st[i]=0;
  }
  if (n==0) return;
  int i=1, c=0;
  while (i<m_n) {
    if (p_st[i]<i) {
      if (i%2==0) swap(p_per[0],p_per[i]);
      else swap(p_per[p_st[i]],p_per[i]);
      if (n==++c) return;
      ++p_st[i];
      i=1;
    }
    else {
      p_st[i]=0;
      ++i;
    }
  }
  return;
}

__device__
double Factorial(int n) {
    return n == 0 ? 1 : n*Factorial(n-1);
}
#endif

__device__
Vec4D Real4(CVec4D in) {
    Vec4D out;
    for(int i = 0; i < 4; ++i) {
        out[i] = in[i].real();
    }
    return out;
}

__device__
void Rambo::GeneratePoint(Event &event, double *rans, double &weight, const int d_nev) {
    Vec4D sump(0., 0., 0., 0.);
    for(size_t i = 0; i < nin; ++i) sump += event.mom[Cur(i)*d_nev];
    double ET = sqrt(sump.Abs2());

    Vec4D R;
    for(size_t i = nin; i < nin+nout; ++i) {
        double ctheta = 2*rans[4*(i-nin)] - 1;
        double stheta = sqrt(1-ctheta*ctheta);
        double phi = 2*M_PI*rans[1+4*(i-nin)];
        double Q = -log(rans[2+4*(i-nin)]*rans[3+4*(i-nin)]); 
        event.mom[Cur(i)*d_nev] = Vec4D(Q, Q*stheta*sin(phi), Q*stheta*cos(phi), Q*ctheta);
        R += event.mom[Cur(i)*d_nev];
    }

    double RMAS = sqrt(R.Abs2());
    Vec3D  B = -Vec3D(R)/RMAS;
    double G = R[0]/RMAS;
    double A = 1.0/(1.0+G);
    double X = ET/RMAS;

    for(size_t i = nin; i < nin+nout; ++i) {
      double e = event.mom[Cur(i)*d_nev][0];
      double BQ = B*Vec3D(event.mom[Cur(i)*d_nev]);
        event.mom[Cur(i)*d_nev] = X*Vec4D((G*e+BQ), Vec3D(event.mom[Cur(i)*d_nev])+B*(e+A*BQ));
#ifdef CDDEBUG
        if(threadIdx.x==0 && blockIdx.x==0) {
	    printf("mom(%d) = {%f,%f,%f,%f}\n", i,
                   event.mom[Cur(i)*d_nev][0],
		   event.mom[Cur(i)*d_nev][1],
		   event.mom[Cur(i)*d_nev][2],
		   event.mom[Cur(i)*d_nev][3]);
        }
#endif
    }
            
    weight = exp((2.*nout - 4.)*log(ET)+Z_N)/pow(2.*M_PI, nout*3. - 4);
    weight *= Cut(event, d_nev);

    double *colors = new double[9];
#ifdef TESTING
    int *color = new int[nin+nout];
    int *acolor = new int[nin+nout];
    int nr = 0, ng = 0, nb = 0;
    for(int i = 0; i < nin+nout; ++i) {
        int idx = 4*nout+4*i;
        if(i < nin) event.mom[Cur(i)*d_nev] = -event.mom[Cur(i)*d_nev];	

        color[i] = int(3*rans[idx]);
        acolor[i] = color[i];
        if(color[i] == 0) nr++;
        if(color[i] == 1) nb++;
        if(color[i] == 2) ng++;
    }

    weight *= pow(3.0, nin+nout)*Factorial(nin+nout)/(Factorial(nr)*Factorial(ng)*Factorial(nb));
    int *swaps = new int[nin+nout];
    Permute(Factorial(nin+nout)*rans[4*nout+(nin+nout)], nin+nout, swaps, acolor);
#endif
    weight *= pow(3.0, nin+nout);
   
    for(int i = 0; i <nin+nout; ++i) {
        int idx = 4*nout+5*i;
        if(i < nin) event.mom[Cur(i)*d_nev] = -event.mom[Cur(i)*d_nev];	
        // Differential elements
        constexpr auto dphase = M_PI;
        constexpr auto dtheta = 2;
        constexpr auto dphi = 2*M_PI;

        // angles for z
        double ctheta1 = dtheta*rans[idx] - 1;
        double stheta1 = sqrt(1-ctheta1*ctheta1);
        double phi1 = dphi*rans[idx+1];
        double ctheta2 = dtheta*rans[idx+2] - 1;
        double stheta2 = sqrt(1-ctheta2*ctheta2);
        double phi2 = dphi*rans[idx+3];

        // Color
        colors[0] = ctheta1*ctheta2;
        colors[1] = ctheta1*cos(phi2)*stheta2;
        colors[2] = ctheta1*sin(phi2)*stheta2;
        colors[3] = ctheta2*cos(phi1)*stheta1;
        colors[4] = stheta1*stheta2*cos(phi1)*cos(phi2);
        colors[5] = stheta1*stheta2*cos(phi1)*sin(phi2);
        colors[6] = ctheta2*sin(phi1)*stheta1;
        colors[7] = stheta1*stheta2*cos(phi2)*sin(phi1);
        colors[8] = stheta1*stheta2*sin(phi2)*sin(phi1);

        // Polarization vector
        thrust::complex<double> phase = thrust::exp(thrust::complex<double>(0, dphase)*rans[idx+4]);
        Vec4D eps = Real4((phase*EP(event.mom[Cur(i)*d_nev]) +
                    thrust::conj(phase)*EM(event.mom[Cur(i)*d_nev])));
#ifdef TESTING
        CVec4D eps;
        if(i < nin) eps = EP(event.mom[Cur(i)*d_nev]);
        else eps = EM(event.mom[Cur(i)*d_nev]);
#endif

        for(int j = 0; j < 3; ++j) {
	    for(int k = 0; k < 3; ++k) {
                event.current[d_nev*CurIdx((1 << i)-1, j, k)] = colors[3*k+j]*eps*sqrt(3.0);
#ifdef TESTING
                if(acolor[i] == k && color[i] == j)
	            event.current[d_nev*CurIdx((1 << i)-1, j, k)] = eps;
                else
                    event.current[d_nev*CurIdx((1 << i)-1, j, k)] = CVec4D();
#endif
            }
        }
    }
#ifdef TESTING
    delete[] swaps;
    delete[] color;
    delete[] acolor;
#endif
    delete[] colors;
}

__global__ void SeedRandom(curandState *state, size_t seed) {
    int id = threadIdx.x+blockIdx.x*blockDim.x;
    curand_init(seed, id, 0, &state[id]);
}

__global__ void GenerateMomenta(curandState *state, Rambo *rambo,
                                Vec4D *mom, Vec4D *eps,
                                double *wgts, double *rans, const int d_nev) {
    int id = threadIdx.x + blockIdx.x*blockDim.x;
    Event event;
    event.max_cur = 1 << (rambo->Npart() - 1);
    event.mom = &mom[id];

    event.current = &eps[id];
    double *thread_rans = rans+(9*rambo->Nout() + 5*rambo->Nin())*id;

    // Copy state to local memory for efficiency
    curandState localState = state[id];

    // Generate random numbers
    for(int i = 0; i < 9*rambo->Nout() + 5*rambo->Nin(); ++i) {
        thread_rans[i] = curand_uniform_double(&localState);
    }

    // Generate momentum and get the event weight
    rambo->GeneratePoint(event, thread_rans, wgts[id], d_nev);  

    // Copy state back to global memory
    state[id] = localState;
}

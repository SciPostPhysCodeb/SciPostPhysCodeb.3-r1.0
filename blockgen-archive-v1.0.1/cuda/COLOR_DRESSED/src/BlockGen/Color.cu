#include "SpinorLib/Color.cuh"
#include "cuda/Interface.cuh"

spinorlib::Color::Color() {
    thrust::complex<double> pI{0, 1};
    thrust::complex<double> mI{0, -1};

    thrust::complex<double> l_colorOperator[72]{
        // T1
        0, 1, 0,
        1, 0, 0,
        0, 0, 0,
        // T2
        0, mI, 0,
        pI, 0, 0,
        0, 0, 0,
        // T3
        1, 0, 0,
        0, -1, 0,
        0, 0, 0,
        // T4
        0, 0, 1,
        0, 0, 0,
        1, 0, 0,
        // T5
        0, 0, mI,
        0, 0, 0,
        pI, 0, 0,
        // T6
        0, 0, 0,
        0, 0, 1,
        0, 1, 0,
        // T7
        0, 0, 0,
        0, 0, mI,
        0, pI, 0,
        // T8
        1.0/sqrt(3.0), 0, 0,
        0, 1.0/sqrt(3.0), 0,
        0, 0, -2.0/sqrt(3.0)
    };

    for(size_t i = 0; i < 72; ++i) {
        colorOperator[i] = l_colorOperator[i];
    }
}

__device__
thrust::complex<double> spinorlib::Color::Taij(unsigned int a, unsigned int i, unsigned int j) {
    size_t idx = j + 3*i + 9*a;
    return colorOperator[idx]/2.0;
}

__device__
void spinorlib::Color::MakeZ(double* phi, double theta, double zeta, zArray &z,
                      thrust::complex<double> pI) {
    z[0] = thrust::exp(pI*phi[0])*cos(theta);
    z[1] = thrust::exp(pI*phi[1])*sin(theta)*cos(zeta);
    z[2] = thrust::exp(pI*phi[2])*sin(theta)*sin(zeta);
}

__device__
void spinorlib::Color::MakeEtaA(zArray z, etaA &eta) {
    const zArray zstr{thrust::conj(z[0]), thrust::conj(z[1]), thrust::conj(z[2])};
    for(unsigned int a = 0; a < 8; ++a) {
        for(unsigned int i = 0; i < 3; ++i) {
            for(unsigned int j = 0; j < 3; ++j) {
                eta[a] += zstr[i]*Taij(a, i, j)*z[j]*sqrt(24.0);
            }
        }
    }
}

__device__
void spinorlib::Color::MakeEtaIJ(zArray z, thrust::complex<double> *eta) {
  const int NC = 3;
  const zArray zstr{thrust::conj(z[0]), thrust::conj(z[1]), thrust::conj(z[2])};
  for(unsigned int i = 0; i < 3; ++i) {
    for(unsigned int j = 0; j < 3; ++j) {
      eta[j+3*i] = zstr[i]*z[j];
    }
    eta[i+3*i] -= static_cast<thrust::complex<double>>(1.0/NC);
  }
}

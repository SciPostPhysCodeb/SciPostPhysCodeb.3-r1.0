#ifndef VECTOR4_HH
#define VECTOR4_HH

#include <functional>

namespace SpinorLib {

namespace detail {

template<typename T>
struct ref_wrap : public std::reference_wrapper<T>{
    operator T&() const noexcept { return this->get(); }
    ref_wrap(T& other) : std::reference_wrapper<T>(other) {}
    void operator=(T &&other) { this -> get()=other; }
};

enum class DataLayout {
    SoA, // Structure of arrays
    AoS, // Array of structures
};

template<typename TContainer>
class Iterator;

template<template<typename...> class Container, DataLayout TDataLayout, typename TItem>
struct DataLayoutPolicy;

template<template<typename...> class Container, template<typename...> class TItem, typename... Types>
struct DataLayoutPolicy<Container, DataLayout::AoS, TItem<Types...>> {
    using type = Container<TItem<Types...>>;
    using value_type = TItem<Types...>&;

    constexpr static value_type get(type &c, std::size_t pos) { 
        return value_type(*static_cast<TItem<Types...>*>(&c[pos])); 
    }

    constexpr static void resize(type &c, std::size_t size) { c.resize(size); }

    template<typename TValue>
    constexpr static void push_back(type &c, )
};


}

#endif

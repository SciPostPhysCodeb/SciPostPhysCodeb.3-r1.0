#pragma once

// Inspiration from: https://stackoverflow.com/a/12382164/9201027 

#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/extrema.h>
#include <cmath>
#include <limits>

// Structure to hold moments
template<typename T>
struct summary_stats_data {
    T n;
    T min;
    T max;
    T mean;
    T m2;

    // initialize to the identity element
    void initialize() {
        n = mean = m2 = 0;
        min = std::numeric_limits<T>::max();
        max = std::numeric_limits<T>::min();
    }

    T variance() { return (m2 - mean*mean) / (n-1); }
    T variance_n() { return m2 / n; }
};

template<typename T>
struct summary_stats_unary_op {
    __host__ __device__
    summary_stats_data<T> operator()(const T &x) const {
        summary_stats_data<T> result;
        result.n = 1;
        result.min = x;
        result.max = x;
        result.mean = x;
        result.m2 = 0;

        return result;
    }
};


template<typename T>
struct summary_stats_binary_op : public thrust::binary_function<const summary_stats_data<T>&,
                                                                const summary_stats_data<T>&,
                                                                summary_stats_data<T> > {
    __host__ __device__
    summary_stats_data<T> operator()(const summary_stats_data<T> &x, const summary_stats_data<T> &y) const {
        summary_stats_data<T> result;

        // precompute some common subexpressions
        T n = x.n + y.n;
        T delta = y.mean - x.mean;
        T delta2 = delta * delta;

        // calculate stats
        result.n = n;
        result.min = thrust::min(x.min, y.min);
        result.max = thrust::max(x.max, y.max);
        result.mean = x.mean + delta * y.n / n;
        result.m2 = x.m2 + y.m2;
        result.m2 += delta2 * x.n * y.n / n;

        return result;
    }
};

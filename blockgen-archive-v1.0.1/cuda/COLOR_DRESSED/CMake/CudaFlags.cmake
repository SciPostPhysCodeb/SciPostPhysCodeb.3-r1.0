# Get Cuda Architecture
set(OUTPUTFILE ${CMAKE_CURRENT_SOURCE_DIR}/cuda_script)
set(CUDAFILE ${CMAKE_CURRENT_SOURCE_DIR}/CMake/check_cuda.cu)
execute_process(COMMAND ${CMAKE_CUDA_COMPILER} -lcuda ${CUDAFILE} -o ${OUTPUTFILE})
execute_process(COMMAND ${OUTPUTFILE}
                RESULT_VARIABLE CUDA_RETURN_CODE
                OUTPUT_VARIABLE ARCH)

if(${CUDA_RETURN_CODE} EQUAL 0)
    set(CUDA_SUCCESS "TRUE")
else()
    set(CUDA_SUCCESS "FALSE")
endif()

if(${CUDA_SUCCESS})
    message(STATUS "CUDA Architecture: ${ARCH}")

    set(CUDA_NVCC_FLAGS "${ARCH}")
else()
    message(FATAL_ERROR "Invalid CUDA Architecture")
endif()

set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} ${CUDA_NVCC_FLAGS}")
#set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -g -G --use_fast_math ${CUDA_NVCC_FLAGS}")

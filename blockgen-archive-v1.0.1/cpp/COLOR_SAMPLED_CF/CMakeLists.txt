# project settings
cmake_minimum_required(VERSION 2.8)
project(ccco)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -O3")

# find external libs
find_package(PkgConfig REQUIRED)
pkg_check_modules(LHAPDF lhapdf>=6.2.1)

# generate config file
configure_file (
  "${PROJECT_SOURCE_DIR}/config.h.in"
  "${PROJECT_BINARY_DIR}/config.h"
  )
include_directories("${PROJECT_BINARY_DIR}")

if(LHAPDF_FOUND)
  link_directories(${LHAPDF_LIBDIR})
endif()

# set floating point precision
if( USE_DOUBLE_PRECISION )
  message("Nout set manually to ${USE_DOUBLE_PRECISION}")
else()
  set(USE_DOUBLE_PRECISION)
  message("Nout set automatically to ${USE_DOUBLE_PRECISION}")
endif()

add_executable(cobg_cpu main.cpp)

if(LHAPDF_FOUND)
  target_link_libraries(cobg_cpu PUBLIC ${LHAPDF_LIBRARIES})
  target_include_directories(cobg_cpu PUBLIC ${LHAPDF_INCLUDE_DIRS})
endif()


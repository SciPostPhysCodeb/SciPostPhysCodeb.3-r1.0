#pragma once
#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <algorithm>
#include "config.h"
#include "../tools/tools.h"

class ColorMatrix{
 public:
  int *cm;
  int n_entries;
  ColorMatrix(const int nn, const std::string filename);
  ~ColorMatrix();
};

ColorMatrix::ColorMatrix(const int nn, const std::string filename){
  std::string line;
  std::ifstream datafile (filename);

  // allocate space for color matrix
  int n_perms = fact(nn-2);
  n_entries = (n_perms * (n_perms+1))/2;
  cm = (int *)malloc(sizeof(int)*n_entries);
  
  // read in matrix
  if(datafile.is_open()){
    for(int i = 0; i<n_entries; i++){
      getline (datafile, line);
      line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
      cm[i] = stoi(line);
    }
    datafile.close();
  }
  else{
    std::cerr << "Could not find ColorMatrix datafile" << std::endl;

    // This produces completely wrong matrix elements, but is suitable to check
    // the memory requirements if the matrix would be known.
    for(int i = 0; i<n_entries; i++){
      cm[i] = 1.;
    }
  }
}
ColorMatrix::~ColorMatrix(){}

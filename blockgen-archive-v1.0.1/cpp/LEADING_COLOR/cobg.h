#pragma once
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <math.h>

#include "config.h"
#include "../tools/vec4.h"

class cobg{
public:
  int n;
  int NN;
  RealMom *J;
  RealMom *P;
  real_float *W;
  
  cobg(int _n){
    n = _n;
    NN = (n-1)*n/2.;
    J = (RealMom*) malloc((NN+2)*sizeof(RealMom));
    P = (RealMom*) malloc((NN+1)*sizeof(RealMom));
    W = (real_float*) malloc((NN+1) * 4 * 4*sizeof(real_float));
  }

  inline int index(int k, int l){
    int m = l-k+1;
    return (m-1)*(n-m/2.) + k;
  }

  inline void V3(RealMom *J, RealMom *P, int a, int b, int c){
    J[c] += 2*(J[a]*P[b])*J[b];
    J[c] -= 2*(P[a]*J[b])*J[a];
    J[c] += (J[a]*J[b])*(P[a]-P[b]);
  }

  inline void V4(RealMom *J, int a, int b, int c, int e){
    J[e] += 2*(J[a]*J[c])*J[b];
    J[e] -= (J[a]*J[b])*J[c];
    J[e] -= (J[c]*J[b])*J[a];
  }
  inline void resetCurrents(){
    for(int i=0; i<NN+1; i++){
      P[i] = {0,0,0,0};
      J[i] = {0,0,0,0};
    }
  }
  inline void reset_offdiag_Currents(){
    for(int i=n-1; i<NN; i++){
      //P[i] = {0,0,0,0};
      J[i] = {0,0,0,0};
    }
  }

  void print_currents(){
    for(int i=0; i<NN+1; i++){
      printf("J[%d] = [%f, %f, %f, %f]\n", i, J[i][0], J[i][1], J[i][2], J[i][3]);
      //printf("P[%d] = [%f, %f, %f, %f]\n", i, P[i][0], P[i][1], P[i][2], P[i][3]);
    }
  }

  void init_P(real_float *moms, const int *shuffle){
    // fill diagonal and off-shell momenta
    for(int i=0; i<n; i++){
      for(int j=0; j<4; j++){
	P[shuffle[i]][j] = moms[i*4+j];
      }
    }
    // fill offdiagonal momenta
    for(int m=1; m<n; m++){
      for(int k=0; k<n-m-1; k++){
	P[index(k,k+m)] = P[index(k,k)] + P[index(k+1,k+m)];
      }
    }
  }
  
  void init_J(real_float *moms,
	       const int *shuffle,
	       const RealMom* polarisation_vectors,
	       const Polarisation *pols){
    reset_offdiag_Currents();

    // fill diagonal and off-shell currents
    for(int i=0; i<n-1; i++){
      J[shuffle[i]] = polarisation_vectors[2*i + static_cast<int>(pols[i])];
    }
    J[NN]   = polarisation_vectors[2*(n-1) + static_cast<int>(0)];
    J[NN+1] = polarisation_vectors[2*(n-1) + static_cast<int>(1)];
  }

  void get_off_shell_currents(const RealMom* polarisation_vectors, RealMom *currents){
    currents[0] = polarisation_vectors[2*(n-1) + static_cast<int>(0)];
    currents[1] = polarisation_vectors[2*(n-1) + static_cast<int>(1)];
  }
  
  void getME(real_float *moms,
	     const int *shuffle,
	     const RealMom* polarisation_vectors,
	     const Polarisation *pols){
    init_J(moms, shuffle, polarisation_vectors, pols);
    
    int currentint;
    for(int m=1; m<n-2; m++){
      for(int k=0; k<n-m-1; k++){
	currentint = index(k,k+m);
	for(int i=k; i<k+m; i++){
	  V3(J, P, index(k,i), index(i+1,k+m), currentint);
	  
	  for(int j = i+1; j<k+m;j++){
	    V4(J, index(k,i), index(i+1,j), index(j+1,k+m), currentint);
	  }
	}
	J[currentint] /= (P[currentint]*P[currentint]);
      }
    }
    for(int m=n-2; m<n; m++){
      for(int k=0; k<n-m-1; k++){
	currentint = index(k,k+m);
	for(int i=k; i<k+m; i++){
	  V3(J, P, index(k,i), index(i+1,k+m), currentint);
    
	  for(int j = i+1; j<k+m;j++){
	    V4(J, index(k,i), index(i+1,j), index(j+1,k+m), currentint);
	  }	  
	}
      }
    }
    //return J[NN-1];
  }

  real_float getME_matrix(real_float *moms,
                          const int *shuffle,
		          const RealMom* polarisation_vectors,
		          const Polarisation *pols){
    init_J(moms, shuffle, polarisation_vectors, pols);

    for(int i=0; i<n; i++){
      for(int mu = 0; mu < 4; mu++){
	for(int nu = 0; nu < 4; nu++){
	  W[16*i + 4*mu + nu] = 2*J[i][mu]*P[i][nu] - P[i][mu]*J[i][nu];
	}
      }
    }
    for(int m=1; m<n; m++){
      for(int k=0; k<n-m-1; k++){
	int mm = k;
	int nn = k+m;
	int i1 = index(mm,nn)*16;

	RealMom tmp = {0,0,0,0};
	for(int i = mm; i<nn; i++){
	  for(int mu = 0; mu<4; mu++){
	    tmp[mu] += W[index(i+1,nn)*16 + 4*mu + 0]*J[index(mm,i)][0]
	      - W[index(i+1,nn)*16 + 4*mu + 1]*J[index(mm,i)][1]
	      - W[index(i+1,nn)*16 + 4*mu + 2]*J[index(mm,i)][2]
	      - W[index(i+1,nn)*16 + 4*mu + 3]*J[index(mm,i)][3];
	    tmp[mu] += -W[index(mm,i)*16 + 4*mu + 0] * J[index(i+1,nn)][0]
	      + W[index(mm,i)*16 + 4*mu + 1] * J[index(i+1,nn)][1]
	      + W[index(mm,i)*16 + 4*mu + 2] * J[index(i+1,nn)][2]
	      + W[index(mm,i)*16 + 4*mu + 3] * J[index(i+1,nn)][3];
	  }
	}
	J[index(mm,nn)] = tmp;
	if(m < n-2){
	  J[index(mm,nn)] /= (P[index(mm,nn)]*P[index(mm,nn)]);
	}

	for(int mu = 0; mu < 4; mu++){
	  for(int nu = 0; nu < 4; nu++){
	    W[i1 + 4*mu + nu] = 2*J[index(mm,nn)][mu]*P[index(mm,nn)][nu]
	      - P[index(mm,nn)][mu]*J[index(mm,nn)][nu];
	    
	    for(int i = mm; i<nn; i++){
	      W[i1 + 4*mu + nu] += J[index(mm,i)][mu]*J[index(i+1,nn)][nu];
	      W[i1 + 4*mu + nu] -= J[index(i+1,nn)][mu]*J[index(mm,i)][nu];
	    }
	    
	  }
	}
      }
    }
    return (J[NN]*J[NN-1]);
  }
};
  

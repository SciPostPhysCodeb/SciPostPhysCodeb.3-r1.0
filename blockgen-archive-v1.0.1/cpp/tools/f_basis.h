#pragma once

#include "config.h"
#include "tools.h"


// color matrix for n = 4
// - NC^2 + NC^4          &&  - 1/2*NC^2 + 1/2*NC^4
// - 1/2*NC^2 + 1/2*NC^4  &&  - NC^2 + NC^4
// 
real_float color_factor(const int i, const int j, const int n, const int NC){
  if(n != 4){
    throw std::invalid_argument("Only implemented for n = 4");
  }
  if(i == j){
    return - pow(NC,2) + pow(NC,4);
  }
  else{
    return - 1./2*pow(NC,2) + 1./2*pow(NC,4);
  }
}

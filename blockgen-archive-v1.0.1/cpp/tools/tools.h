#ifndef TOOLSH
#define TOOLSH

#include "config.h"
#include <iostream>
#include <math.h>
#include <cstdlib>

// creates random integers
// uniformly distributed in the range [i,j).
inline int getRand(const int i, const int j){
  return i + std::rand()*1./RAND_MAX *(j-i);
}

int fact(const int n){
  return (n == 1 || n == 0) ? 1 : fact(n - 1) * n;
}

// Implementing a suitable version of the modern Fisher-Yates shuffle
//  for i from 0 to n−1 do
//     j ← random integer such that i ≤ j < n
//     exchange a[i] and a[j]
//  Src: https://en.wikipedia.org/wiki/Fisher-Yates_shuffle

// returning array with random shuffe except in the first position
//      [1,2,3,...,n] --> [1,r2,r3,...,rn]
void shuffle(const int n, int *a){
  int j;
  real_float tmp;
  for(int i = 0; i<n; i++){
    j = getRand(i,n-1);
    tmp = a[j];
    a[j] = a[i];
    a[i] = tmp;
  }
}

void full_shuffle(const int n, int *a){
  int j;
  real_float tmp;
  for(int i = 0; i<n; i++){
    j = getRand(i,n);
    tmp = a[j];
    a[j] = a[i];
    a[i] = tmp;
  }
}

#endif

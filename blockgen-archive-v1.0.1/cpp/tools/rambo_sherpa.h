#include <iostream>
#include <math.h>
#include <cstdlib>


class RamboSherpa{
  double ecm;
  double Z_N;
  int nin;
  int nout;
    
public:
  RamboSherpa(int _nin, int _nout, double ecms);
  inline double getRand();
  void generatePoint(double *p, const double x1, const double x2);
  double generateWeight(double *p, double x1, double x2);
      
};

#include "rambo_sherpa.h"	

inline double RamboSherpa::getRand(){
  return std::rand()*1./RAND_MAX;
}

RamboSherpa::RamboSherpa(int _nin, int _nout, double ecms){
  nin = _nin;
  nout = _nout;
  ecm = ecms;

  // inspired by SHERPA-MC-2.2.6/PHASIC++/Channels/Rambo.C
  double   pi2log = log(M_PI/2.);
  double * Z      = new double[nout+1];
  Z[2] = pi2log;
  for (short int k=3;k<=nout;k++) Z[k] = Z[k-1]+pi2log-2.*log(double(k-2));
  for (short int k=3;k<=nout;k++) Z[k] = Z[k]-log(double(k-1));
  Z_N  = Z[nout];
  delete[] Z;
}

void RamboSherpa::generatePoint(double *p, const double x1, const double x2){
  p[0] = -x1 * ecm/2.;
  p[1] = 0;
  p[2] = 0;
  p[3] = -x1 * ecm/2.;
  p[4] = -x2 * ecm/2.;
  p[5] = 0;
  p[6] = 0;
  p[7] = x2 * ecm/2.;

  double sump[4] = {0,0,0,0};
  for (int i=0; i<nin; i++){
    for (int j=0; j<4; j++){
      sump[j] += p[4*i+j];
    }
  }
  double ET = sqrt(pow(sump[0],2) - pow(sump[1],2) - pow(sump[2],2) - pow(sump[3],2));

  double Q, S, C, F, G, A, X, RMAS, BQ, e;
  short int i;
  double R[4] = {0,0,0,0};
  double B[4];

  for(i=nin;i<nin+nout;i++){
    C = 2*getRand() -1;
    S = sqrt(1-C*C);
    F = 2*M_PI*getRand();

    Q = -log(std::min(1.0-1.e-10,
		      std::max(1.e-10,getRand()*getRand())));
    p[4*i+0] = Q;
    p[4*i+1] = Q*S*sin(F);
    p[4*i+2] = Q*S*cos(F);
    p[4*i+3] = Q*C;
    for(int j = 0; j<4; j++){
      R[j] += p[4*i+j];
    }
  }
  
  RMAS = sqrt(pow(R[0],2) - pow(R[1],2) - pow(R[2],2) - pow(R[3],2));
  
  B[0] = 0;
  B[1] = -R[1]/RMAS;
  B[2] = -R[2]/RMAS;
  B[3] = -R[3]/RMAS;
  G = R[0]/RMAS;
  A = 1./(1.+G);
  X = ET*1./RMAS;
  
  for(i=nin;i<nin+nout;i++){
    e = p[4*i + 0];
    BQ = B[1] * p[4*i+1] + B[2] * p[4*i+2] + B[3] * p[4*i+3];
    
    p[4*i+0] = X * (G*e+BQ);
    p[4*i+1] = X * (p[4*i+1] + B[1]*(e+A*BQ));
    p[4*i+2] = X * (p[4*i+2] + B[2]*(e+A*BQ));
    p[4*i+3] = X * (p[4*i+3] + B[3]*(e+A*BQ));
  }
}

double RamboSherpa::generateWeight(double *p, double x1, double x2){
  // inspired by SHERPA-MC-2.2.6/PHASIC++/Channels/Rambo.C
  const double w = sqrt(x1*x2)*ecm;
  const double weight = exp((2.*nout-4.)*log(w)+Z_N)/pow(2.*M_PI,nout*3.-4.);
  return weight;
}


#ifndef VEC4H
#define VEC4H

#include <math.h>
#include <complex>
#include <array>
#include "config.h"

template <typename T> struct Mom {
  std::array<T, 4> data;

  T &operator[](int i) { return data[i]; }
  T operator[](int i) const { return data[i]; }

  // turn a complex momentum with vanishing imaginary parts to an explitily
  // real-valued momentum
  /*
  friend Mom<real> real(const Mom &m) {
    Mom<real> real_mom;
    for (int i{0}; i < 4; ++i) {
      assert(m[i].imag() < 1e-10);
      real_mom[i] = m[i].real();
    }
    return real_mom;
  }
  */

  friend Mom conj(const Mom &m) {
    return {std::conj(m[0]), std::conj(m[1]), std::conj(m[2]), std::conj(m[3])};
  }

  Mom &operator+=(const Mom &rhs) {
    for (int i{0}; i < 4; ++i)
      (*this)[i] += rhs[i];
    return *this;
  }
  friend Mom operator+(Mom lhs, const Mom &rhs) {
    lhs += rhs;
    return lhs;
  }

  Mom &operator-=(const Mom &rhs) {
    for (int i{0}; i < 4; ++i)
      (*this)[i] -= rhs[i];
    return *this;
  }
  friend Mom operator-(Mom lhs, const Mom &rhs) {
    lhs -= rhs;
    return lhs;
  }

  // multiplication with a scalar
  template <typename U> Mom &operator*=(const U &rhs) {
    for (int i{0}; i < 4; ++i)
      (*this)[i] *= rhs;
    return *this;
  }
  template <typename U> Mom &operator/=(const U &rhs) {
    for (int i{0}; i < 4; ++i)
      (*this)[i] /= rhs;
    return *this;
  }

  template <typename U> friend Mom operator*(Mom lhs, const U &rhs) {
    lhs *= rhs;
    return lhs;
  }
  template <typename U> friend Mom operator*(const U &lhs, Mom rhs) {
    rhs *= lhs;
    return rhs;
  }
  
  template <typename U> friend Mom operator/(Mom lhs, const U &rhs) {
    lhs /= rhs;
    return lhs;
  }
  
  // Minkowski scalar product
  friend T operator*(const Mom &lhs, const Mom &rhs) {
    T norm{lhs[0] * rhs[0]};
    for (int i{1}; i < 4; ++i)
      norm -= lhs[i] * rhs[i];
    return norm;
  }

  friend bool almost_equal(const Mom &lhs, const Mom &rhs) {
    for (int i{1}; i < 4; ++i)
      if (!almost_equal(lhs[i], rhs[i]))
        return false;
    return true;
  }

  friend std::ostream &operator<<(std::ostream &out, const Mom &m) {
    out << '(';
    for (int i{0}; i < 3; ++i)
      out << m[i] << ", ";
    out << m[3] << ')';
    return out;
  }
};

using RealMom = Mom<real_float>;
using ComplexMom = Mom<std::complex<real_float>>;

enum class Polarisation { xlin=0, ylin=1 };

void fill_real_polarisation_vectors(RealMom* pols, const real_float* moms, size_t count) {
  // build two real orthogonal polarisation vectors for each p = mom[j] (up
  // to j = count - 1), and fill them into pols[j][0] and pols[j][1]

  for (int j = 0; j < count; j++) {

    // calculate k = p / |p|
    RealMom k;
    k[0] = 0;
    k[1] = moms[4*j+1];
    k[2] = moms[4*j+2];
    k[3] = moms[4*j+3];
    k /= 1.*std::sqrt(pow(k[1],2)+pow(k[2],2)+pow(k[3],2));

    // order momenta
    int xi[4] = {0,1,2,3};
    int maxind = 1;
    for(int i = 2; i<4; i++){
      maxind = (std::abs(k[i]) > std::abs(k[maxind])) ? i : maxind;
    }
    xi[maxind] = 1;
    xi[1] = maxind;
    real_float norm = std::sqrt(k[xi[1]]*k[xi[1]] + k[xi[2]]*k[xi[2]]);

    // construct normalised vector orthogonal to k and fill into pols[j+0]
    pols[2*j + static_cast<int>(Polarisation::xlin)][0] = 0;
    pols[2*j + static_cast<int>(Polarisation::xlin)][xi[1]] =  k[xi[2]];
    pols[2*j + static_cast<int>(Polarisation::xlin)][xi[2]] = -k[xi[1]];
    pols[2*j + static_cast<int>(Polarisation::xlin)][xi[3]] = 0;
    pols[2*j + static_cast<int>(Polarisation::xlin)] /= norm;

    // Construct vector orthogonal to k and pols[j+0] and fill into pols[j+1]
    pols[2*j + static_cast<int>(Polarisation::ylin)][0] = 0;
    pols[2*j + static_cast<int>(Polarisation::ylin)][xi[1]] = k[xi[1]] * k[xi[3]] / norm;
    pols[2*j + static_cast<int>(Polarisation::ylin)][xi[2]] = k[xi[2]] * k[xi[3]] / norm;
    pols[2*j + static_cast<int>(Polarisation::ylin)][xi[3]] = -norm;

  }
}

void select_random_polarisations(RealMom* pols, size_t count) {
  // selects a random linear combination for each pair of two real orthogonal
  // polarisation vectors; count gives the number of such pairs the result will
  // be put into the first pair slot, while the second slot will be set to zero
  for (int j = 0; j < count; j++) {
      const real_float theta = std::rand()*2./RAND_MAX * M_PI;
      pols[2*j+0] =
	std::cos(theta) * pols[2*j+0] +
	std::sin(theta) * pols[2*j+1];
      pols[2*j+1] = {0.0, 0.0, 0.0, 0.0};
  }
}

#endif

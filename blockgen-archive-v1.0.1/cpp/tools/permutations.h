#pragma once

#include "config.h"
#include "tools.h"
#include <vector>

class Permutations{
  int n;
  
 public:
  std::vector< std::vector<int> > perms;
  Permutations(const int _n);
  void createPerms();
  void createPermsLexi();
  std::vector<int> nextPerm();
};

Permutations::Permutations(const int _n){
  n = _n;
}

// Modified Version of Heap*s algorithm
//
// Suitable for the F-Basis computations, i.e. containing all permutations of
// (2,...,n-1) with 1, n fixed.
//
// https://en.wikipedia.org/wiki/Heap's_algorithm
void Permutations::createPerms(){
  perms.clear();

  std::vector<int> A;
  int c[n];
  for(int i = 0; i<n; i++){
    A.push_back(i);
    c[i] = 1;
  }
  std::vector<int> perm(A);
  perms.push_back(perm);
  
  int i = 1;
  while(i<n-1){
    if(c[i] < i){
      if(i % 2 == 1){
	int tmp = A[1];
	A[1] = A[i];
	A[i] = tmp;
      }
      else{
	int tmp = A[c[i]];
	A[c[i]] = A[i];
	A[i] = tmp;
      }
      std::vector<int> perm2(A);
      perms.push_back(perm2);
      ++c[i];
      i = 1;
    }
    else{
      c[i] = 1;
      ++i;
    }
  }
}

void Permutations::createPermsLexi(){
  perms.clear();

  std::vector<int> A;
  for(int i = 0; i<n; i++){
    A.push_back(i);
  }
  std::vector<int> perm(A);
  perms.push_back(perm);
  
  while (true){
    int index = 0;
    for(int i = 0; i<n-2; i++){
      if(A[i]<A[i+1]) {	index = i; }
    }
    if (index == 0){ break; }

    int index2 = 0;
    for(int i = index+1; i<n-1; i++){
      if (A[index] < A[i]){ index2 = i; }
    }

    int tmp   = A[index];
    A[index]  = A[index2];
    A[index2] = tmp;

    for(int i = 0; i<(n-1-(index+1))/2; i++){
      int tmp2 = A[index+1+i];
      A[index+1+i] = A[n-2-i];
      A[n-2-i] = tmp2;
    }
    std::vector<int> perm2(A);
    perms.push_back(perm2);
  }
}
